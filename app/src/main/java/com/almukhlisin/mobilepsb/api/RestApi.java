package com.almukhlisin.mobilepsb.api;

import java.util.List;

import com.almukhlisin.mobilepsb.model.cons.Endpoint;
import com.almukhlisin.mobilepsb.model.data.Akun;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.Daful;
import com.almukhlisin.mobilepsb.model.data.DetailPendaftar;
import com.almukhlisin.mobilepsb.model.data.JadwalUjian;
import com.almukhlisin.mobilepsb.model.data.Kuota;
import com.almukhlisin.mobilepsb.model.data.Nilai;
import com.almukhlisin.mobilepsb.model.data.Pendaftar;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.model.data.Tren;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;


public interface RestApi {

    @FormUrlEncoded
    @POST(Endpoint.POST_LOGIN)
    Call<ResultData<List<Auth>>> postLogin(@Field("username") String username,
                                           @Field("password") String password);

    @GET(Endpoint.GET_AKUN)
    Call<ResultData<List<Akun>>> getAkunByUsername(@Query("username") String username);

    @GET(Endpoint.GET_AKUN)
    Call<ResultData<List<Akun>>> getListAkun(@Query("limit") String limit);

    @GET(Endpoint.GET_PENDAFTAR)
    Call<ResultData<List<Pendaftar>>> getListPendaftar(@Query("field") String field,
                                                       @Query("search") String search,
                                                       @Query("limit") String limit,
                                                       @Query("verified") String verified,
                                                       @Query("username") String username,
                                                       @Query("id_calon") String idCalon);
    @GET(Endpoint.GET_DETAIL_PENDAFTAR)
    Call<ResultData<List<DetailPendaftar>>> getListDetailPendaftar(@Query("field") String field,
                                                                   @Query("search") String search,
                                                                   @Query("limit") String limit,
                                                                   @Query("verified") String verified,
                                                                   @Query("username") String username,
                                                                   @Query("id_calon") String idCalon);

    @FormUrlEncoded
    @POST(Endpoint.POST_VERIFY_PENDAFTAR)
    Call<Result> postVerifyPendaftar(@Field("id_calon") String id_calon);

    @FormUrlEncoded
    @POST(Endpoint.POST_VERIFY_DAFTAR_ULANG)
    Call<Result> postVerifyDaftarUlang(@Field("id_calon") String id_calon);

    @FormUrlEncoded
    @POST(Endpoint.POST_INSERT_PENDAFTAR)
    Call<Result> postInsertPendaftar(@Field("email") String email, @Field("jenis_pendaftaran") String jenisPendaftaran,
                                         @Field("nama_calon") String namaCalon, @Field("jenis_kelamin") String jenisKelamin,
                                         @Field("ttl") String ttl, @Field("agama") String agama,
                                         @Field("sekolah_asal") String sekolahAsal, @Field("tahun_ijazah") String tahunIjazah,
                                         @Field("alamat") String alamat, @Field("anak") String anak,
                                         @Field("jumlah_saudara") String jumlahSaudara, @Field("nama_ayah") String namaAyah,
                                         @Field("nama_ibu") String namaIbu, @Field("pekerjaan_ayah") String pekerjaanAyah,
                                         @Field("pekerjaan_ibu") String pekerjaanIbu, @Field("alamat_ortu") String alamatOrtu,
                                         @Field("pendidikan_ayah") String pendidikanAyah, @Field("pendidikan_ibu") String pendidikanIbu,
                                         @Field("no_hp") String noHp, @Field("password") String password);
    @FormUrlEncoded
    @POST(Endpoint.POST_UPDATE_PENDAFTAR)
    Call<Result> postUpdatePendaftar(@Field("id_calon") String id_calon, @Field("email") String email,
                                     @Field("jenis_pendaftaran") String jenisPendaftaran, @Field("nama_calon") String namaCalon,
                                     @Field("jenis_kelamin") String jenisKelamin, @Field("ttl") String ttl, @Field("agama") String agama,
                                     @Field("sekolah_asal") String sekolahAsal, @Field("tahun_ijazah") String tahunIjazah,
                                     @Field("alamat") String alamat, @Field("anak") String anak,
                                     @Field("jumlah_saudara") String jumlahSaudara, @Field("nama_ayah") String namaAyah,
                                     @Field("nama_ibu") String namaIbu, @Field("pekerjaan_ayah") String pekerjaanAyah,
                                     @Field("pekerjaan_ibu") String pekerjaanIbu, @Field("alamat_ortu") String alamatOrtu,
                                     @Field("pendidikan_ayah") String pendidikanAyah, @Field("pendidikan_ibu") String pendidikanIbu,
                                     @Field("no_hp") String noHp, @Field("password") String password);

    @POST(Endpoint.UPLOAD_IMAGE)
    Call<Result> postUploadImage(@Query("id") String id, @Query("type") String type,
                                 @Body RequestBody body);

    @GET(Endpoint.GET_JADWAL_UJIAN)
    Call<ResultData<List<JadwalUjian>>> getListJadwalUjian(@Query("status_jadwal") String statusJadwal,
                                                       @Query("jenis_pendaftaran") String jenis_pendaftaran);

    @FormUrlEncoded
    @POST(Endpoint.POST_DELETE_JADWAL_UJIAN)
    Call<Result> postDeleteJadwalUjian(@Field("id_jadwal") String idJadwal);

    @FormUrlEncoded
    @POST(Endpoint.POST_INSERT_JADWAL_UJIAN)
    Call<Result> postInsertJadwalUjian(@Field("jenis_ujian") String jenisUjian,
                                       @Field("tanggal_ujian") String tanggalUjian,
                                       @Field("waktu_ujian") String waktuUjian,
                                       @Field("jenis_pendaftaran") String jenisPendaftaran,
                                       @Field("ruangan") String ruangan);

    @GET(Endpoint.GET_NILAI)
    Call<ResultData<List<Nilai>>> getListNilai(@Query("field") String field,
                                               @Query("search") String search,
                                               @Query("limit") String limit,
                                               @Query("id_calon") String idCalon,
                                               @Query("status_kelulusan") String statusKelulusan);

    @GET(Endpoint.GET_DAFTAR_ULANG)
    Call<ResultData<List<Daful>>> getListDaftarUlang(@Query("field") String field,
                                               @Query("search") String search,
                                               @Query("limit") String limit,
                                               @Query("id_calon") String idCalon,
                                               @Query("status") String status);

    @GET(Endpoint.GET_TREN)
    Call<ResultData<List<Tren>>> getListTren();

    @POST(Endpoint.POST_UPDATE_PENGUMUMAN)
    Call<Result> postUpdatePengumuman();

    @FormUrlEncoded
    @POST(Endpoint.POST_DELETE_AKUN)
    Call<Result> postDeleteAkun(@Field("username") String username);

    @FormUrlEncoded
    @POST(Endpoint.POST_DELETE_KUOTA)
    Call<Result> postDeleteKuota(@Field("id_kuota") String idKuota);

    @FormUrlEncoded
    @POST(Endpoint.POST_INSERT_KUOTA)
    Call<Result> postInsertKuota(@Field("jenis_pendaftaran") String jenisPendaftaran,
                                 @Field("tahun") String tahun,
                                 @Field("jumlah") String jumlah);

    @GET(Endpoint.GET_KUOTA)
    Call<ResultData<List<Kuota>>> getListKuota(@Query("field") String field,
                                               @Query("search") String search,
                                               @Query("limit") String limit,
                                               @Query("tahun") String tahun);

    @FormUrlEncoded
    @POST(Endpoint.POST_INSERT_AKUN)
    Call<Result> postInsertAkun(@Field("username") String username,
                                @Field("password") String password,
                                @Field("email") String email,
                                @Field("nama_lengkap") String namaLengkap,
                                @Field("no_hp") String noHp,
                                @Field("level") String level);


    @FormUrlEncoded
    @POST(Endpoint.POST_DELETE_NILAI)
    Call<Result> postDeleteNilai(@Field("id_calon") String idCalon);

    @FormUrlEncoded
    @POST(Endpoint.POST_INSERT_DAFTAR_ULANG)
    Call<Result> postInsertDaftarUlang(@Field("id_calon") String idCalon);

    @FormUrlEncoded
    @POST(Endpoint.POST_INSERT_NILAI)
    Call<Result> postInsertNilai(@Field("id_calon") String idCalon,
                                       @Field("nilai_baca") String nilaiBaca,
                                       @Field("nilai_tulis") String nilaiTulis);
}
