package com.almukhlisin.mobilepsb.model.other;

import java.io.File;
import java.net.URI;

/**
 * Created by qornanali on 7/26/17.
 */

public class Attachment {

    private String description = "";
    private String url = "";
    private String id = "";
    private File file;



    public File getFile() {
        return file;
    }

    public void setFile(String url) {
        this.file = new File(url);
    }

    public Attachment(String description, String url) {
        this.description = description;
        this.url = url;
    }

    public Attachment(String description, String url, String id) {
        this.description = description;
        this.url = url;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
