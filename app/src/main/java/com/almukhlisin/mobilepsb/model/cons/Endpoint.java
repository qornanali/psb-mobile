package com.almukhlisin.mobilepsb.model.cons;

public class Endpoint {

    public static final String POST_LOGIN = "post_login.php";
    public static final String UPLOAD_IMAGE = "upload_image.php";
    public static final String GET_TREN = "tren/get_tren.php";

    public static final String GET_AKUN = "akun/get_akun.php";
    public static final String POST_DELETE_AKUN = "akun/post_delete_akun.php";
    public static final String POST_INSERT_AKUN = "akun/post_insert_akun.php";

    public static final String GET_KUOTA = "kuota/get_kuota.php";
    public static final String POST_DELETE_KUOTA = "kuota/post_delete_kuota.php";
    public static final String POST_INSERT_KUOTA = "kuota/post_insert_kuota.php";

    public static final String POST_UPDATE_PENGUMUMAN = "pengumuman/post_update_pengumuman.php";

    public static final String GET_DAFTAR_ULANG = "daftar_ulang/get_daftar_ulang.php";
    public static final String POST_INSERT_DAFTAR_ULANG = "daftar_ulang/post_insert_daftar_ulang.php";
    public static final String POST_VERIFY_DAFTAR_ULANG = "daftar_ulang/post_verify_daftar_ulang.php";

    public static final String GET_JADWAL_UJIAN = "jadwal_ujian/get_jadwal_ujian.php";
    public static final String POST_INSERT_JADWAL_UJIAN = "jadwal_ujian/post_insert_jadwal_ujian.php";
    public static final String POST_DELETE_JADWAL_UJIAN = "jadwal_ujian/post_delete_jadwal_ujian.php";

    public static final String GET_NILAI = "nilai/get_nilai.php";
    public static final String POST_INSERT_NILAI = "nilai/post_insert_nilai.php";
    public static final String POST_DELETE_NILAI = "nilai/post_delete_nilai.php";

    public static final String GET_PENDAFTAR = "pendaftar/get_pendaftar.php";
    public static final String GET_DETAIL_PENDAFTAR = "pendaftar/get_detail_pendaftar.php";
    public static final String POST_VERIFY_PENDAFTAR = "pendaftar/post_verify_pendaftar.php";
    public static final String POST_INSERT_PENDAFTAR = "pendaftar/post_insert_pendaftar.php";
    public static final String POST_UPDATE_PENDAFTAR = "pendaftar/post_update_pendaftar.php";

}
