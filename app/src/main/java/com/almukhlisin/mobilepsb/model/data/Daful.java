package com.almukhlisin.mobilepsb.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by qornanali on 7/29/17.
 */

public class Daful {

    @SerializedName("id_daftarulang")
    private String idDaftarUlang;
    @SerializedName("id_calon")
    private String idCalon;
    @SerializedName("jenis_pendaftaran")
    private String jenisPendaftaran;
    private String bukti;
    @SerializedName("tanggal_pembayaran")
    private String tanggalPembayaran;
    private String status;

    public String getIdDaftarUlang() {
        return idDaftarUlang;
    }

    public void setIdDaftarUlang(String idDaftarUlang) {
        this.idDaftarUlang = idDaftarUlang;
    }

    public String getIdCalon() {
        return idCalon;
    }

    public void setIdCalon(String idCalon) {
        this.idCalon = idCalon;
    }

    public String getJenisPendaftaran() {
        return jenisPendaftaran;
    }

    public void setJenisPendaftaran(String jenisPendaftaran) {
        this.jenisPendaftaran = jenisPendaftaran;
    }

    public String getBukti() {
        return bukti;
    }

    public void setBukti(String bukti) {
        this.bukti = bukti;
    }

    public String getTanggalPembayaran() {
        return tanggalPembayaran;
    }

    public void setTanggalPembayaran(String tanggalPembayaran) {
        this.tanggalPembayaran = tanggalPembayaran;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Daful{" +
                "status='" + status + '\'' +
                ", tanggalPembayaran='" + tanggalPembayaran + '\'' +
                ", bukti='" + bukti + '\'' +
                ", jenisPendaftaran='" + jenisPendaftaran + '\'' +
                ", idCalon='" + idCalon + '\'' +
                ", idDaftarUlang='" + idDaftarUlang + '\'' +
                '}';
    }
}
