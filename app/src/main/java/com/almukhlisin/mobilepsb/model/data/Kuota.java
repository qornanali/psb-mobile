package com.almukhlisin.mobilepsb.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by qornanali on 8/1/17.
 */

public class Kuota {

    @SerializedName("id_kuota")
    private String idKuota;
    @SerializedName("jenis_pendaftaran")
    private String jenisPendaftaran;
    private String tahun;
    private String jumlah;

    public String getIdKuota() {
        return idKuota;
    }

    public void setIdKuota(String idKuota) {
        this.idKuota = idKuota;
    }

    public String getJenisPendaftaran() {
        return jenisPendaftaran;
    }

    public void setJenisPendaftaran(String jenisPendaftaran) {
        this.jenisPendaftaran = jenisPendaftaran;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    @Override
    public String toString() {
        return "Kuota{" +
                "idKuota='" + idKuota + '\'' +
                ", jenisPendaftaran='" + jenisPendaftaran + '\'' +
                ", tahun='" + tahun + '\'' +
                ", jumlah='" + jumlah + '\'' +
                '}';
    }
}
