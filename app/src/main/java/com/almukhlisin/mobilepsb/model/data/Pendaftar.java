package com.almukhlisin.mobilepsb.model.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Pendaftar implements Serializable {

    @SerializedName("id_calon")
    private String idCalon;
    @SerializedName("nama_calon")
    private String namaCalon;
    @SerializedName("sekolah_asal")
    private String sekolahAsal;
    @SerializedName("pas_foto")
    private String pasFoto;
    @SerializedName("status_pendaftaran")
    private String statusPendaftaran;
    private String email;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdCalon() {
        return idCalon;
    }

    public void setIdCalon(String idCalon) {
        this.idCalon = idCalon;
    }

    public String getNamaCalon() {
        return namaCalon;
    }

    public void setNamaCalon(String namaCalon) {
        this.namaCalon = namaCalon;
    }

    public String getSekolahAsal() {
        return sekolahAsal;
    }

    public void setSekolahAsal(String sekolahAsal) {
        this.sekolahAsal = sekolahAsal;
    }

    public String getPasFoto() {
        return pasFoto;
    }

    public void setPasFoto(String pasFoto) {
        this.pasFoto = pasFoto;
    }


    public String getStatusPendaftaran() {
        return statusPendaftaran;
    }

    public void setStatusPendaftaran(String statusPendaftaran) {
        this.statusPendaftaran = statusPendaftaran;
    }

    @Override
    public String toString() {
        return "Pendaftar{" +
                "pasFoto='" + pasFoto + '\'' +
                ", sekolahAsal='" + sekolahAsal + '\'' +
                ", namaCalon='" + namaCalon + '\'' +
                ", idCalon='" + idCalon + '\'' +
                ", email='" + email + '\'' +
                ", statusPendaftaran='" + statusPendaftaran + '\'' +
                '}';
    }
}
