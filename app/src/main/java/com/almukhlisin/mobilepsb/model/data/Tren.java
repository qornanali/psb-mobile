package com.almukhlisin.mobilepsb.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by qornanali on 8/1/17.
 */

public class Tren {

    @SerializedName("tren_title")
    private String trenTitle;
    @SerializedName("tren_content")
    private String trenContent;

    public Tren(String trenTitle, String trenContent) {
        this.trenTitle = trenTitle;
        this.trenContent = trenContent;
    }

    public String getTrenTitle() {
        return trenTitle;
    }

    public void setTrenTitle(String trenTitle) {
        this.trenTitle = trenTitle;
    }

    public String getTrenContent() {
        return trenContent;
    }

    public void setTrenContent(String trenContent) {
        this.trenContent = trenContent;
    }
}
