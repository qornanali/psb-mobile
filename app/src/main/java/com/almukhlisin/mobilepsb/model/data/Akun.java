package com.almukhlisin.mobilepsb.model.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Akun {

    private String username;
    @SerializedName("nama_lengkap")
    private String namaLengkap;
    private String email;
    @SerializedName("no_hp")
    private String noHp;
    private String level;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
