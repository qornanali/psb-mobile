package com.almukhlisin.mobilepsb.model.other;


public class DataDetailPendaftar {

    private String content;
    private String field;

    public DataDetailPendaftar(String content, String field) {
        this.content = content;
        this.field = field;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
