package com.almukhlisin.mobilepsb.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by qornanali on 7/28/17.
 */

public class JadwalUjian {

    @SerializedName("id_jadwal")
    private String idJadwal;
    @SerializedName("jenis_ujian")
    private String jenisUjian;
    @SerializedName("tanggal_ujian")
    private String tanggalUjian;
    @SerializedName("waktu_ujian")
    private String waktuUjian;
    @SerializedName("jenis_pendaftaran")
    private String jenisPendaftaran;
    private String ruangan;
    @SerializedName("status_jadwal")
    private String statusJadwal;

    public String getIdJadwal() {
        return idJadwal;
    }

    public void setIdJadwal(String idJadwal) {
        this.idJadwal = idJadwal;
    }

    public String getJenisUjian() {
        return jenisUjian;
    }

    public void setJenisUjian(String jenisUjian) {
        this.jenisUjian = jenisUjian;
    }

    public String getTanggalUjian() {
        return tanggalUjian;
    }

    public void setTanggalUjian(String tanggalUjian) {
        this.tanggalUjian = tanggalUjian;
    }

    public String getWaktuUjian() {
        return waktuUjian;
    }

    public void setWaktuUjian(String waktuUjian) {
        this.waktuUjian = waktuUjian;
    }

    public String getJenisPendaftaran() {
        return jenisPendaftaran;
    }

    public void setJenisPendaftaran(String jenisPendaftaran) {
        this.jenisPendaftaran = jenisPendaftaran;
    }

    public String getRuangan() {
        return ruangan;
    }

    public void setRuangan(String ruangan) {
        this.ruangan = ruangan;
    }

    public String getStatusJadwal() {
        return statusJadwal;
    }

    public void setStatusJadwal(String statusJadwal) {
        this.statusJadwal = statusJadwal;
    }

    @Override
    public String toString() {
        return "JadwalUjian{" +
                "idJadwal='" + idJadwal + '\'' +
                ", jenisUjian='" + jenisUjian + '\'' +
                ", tanggalUjian='" + tanggalUjian + '\'' +
                ", waktuUjian='" + waktuUjian + '\'' +
                ", jenisPendaftaran='" + jenisPendaftaran + '\'' +
                ", ruangan='" + ruangan + '\'' +
                ", statusJadwal='" + statusJadwal + '\'' +
                '}';
    }
}
