package com.almukhlisin.mobilepsb.model.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DetailPendaftar extends Pendaftar implements Serializable {

    @SerializedName("jenis_pendaftaran")
    private String jenisPendaftaran;
    @SerializedName("jenis_kelamin")
    private String jenis_kelamin;
    private String ttl;
    private String agama;
    @SerializedName("tahun_ijazah")
    private String tahunIjazah;
    private String alamat;
    private String anak;
    @SerializedName("jumlah_saudara")
    private String jumlahSaudara;
    @SerializedName("nama_ayah")
    private String namaAyah;
    @SerializedName("nama_ibu")
    private String namaIbu;
    @SerializedName("pekerjaan_ayah")
    private String pekerjaanAyah;
    @SerializedName("pekerjaan_ibu")
    private String pekerjaanIbu;
    @SerializedName("pendidikan_ayah")
    private String pendidikanAyah;
    @SerializedName("pendidikan_ibu")
    private String pendidikanIbu;
    @SerializedName("alamat_ortu")
    private String alamatOrtu;
    @SerializedName("no_hp")
    private String noHp;
    @SerializedName("foto_ijazah")
    private String fotoIjazah;
    @SerializedName("foto_skhun")
    private String fotoSkhun;
    private String akte;
    @SerializedName("kartu_keluarga")
    private String kartuKeluarga;


    public String getJenisPendaftaran() {
        return jenisPendaftaran;
    }

    public void setJenisPendaftaran(String jenisPendaftaran) {
        this.jenisPendaftaran = jenisPendaftaran;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getTahunIjazah() {
        return tahunIjazah;
    }

    public void setTahunIjazah(String tahunIjazah) {
        this.tahunIjazah = tahunIjazah;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getAnak() {
        return anak;
    }

    public void setAnak(String anak) {
        this.anak = anak;
    }

    public String getJumlahSaudara() {
        return jumlahSaudara;
    }

    public void setJumlahSaudara(String jumlahSaudara) {
        this.jumlahSaudara = jumlahSaudara;
    }

    public String getNamaAyah() {
        return namaAyah;
    }

    public void setNamaAyah(String namaAyah) {
        this.namaAyah = namaAyah;
    }

    public String getNamaIbu() {
        return namaIbu;
    }

    public void setNamaIbu(String namaIbu) {
        this.namaIbu = namaIbu;
    }

    public String getPekerjaanAyah() {
        return pekerjaanAyah;
    }

    public void setPekerjaanAyah(String pekerjaanAyah) {
        this.pekerjaanAyah = pekerjaanAyah;
    }

    public String getPekerjaanIbu() {
        return pekerjaanIbu;
    }

    public void setPekerjaanIbu(String pekerjaanIbu) {
        this.pekerjaanIbu = pekerjaanIbu;
    }

    public String getPendidikanAyah() {
        return pendidikanAyah;
    }

    public void setPendidikanAyah(String pendidikanAyah) {
        this.pendidikanAyah = pendidikanAyah;
    }

    public String getPendidikanIbu() {
        return pendidikanIbu;
    }

    public void setPendidikanIbu(String pendidikanIbu) {
        this.pendidikanIbu = pendidikanIbu;
    }

    public String getAlamatOrtu() {
        return alamatOrtu;
    }

    public void setAlamatOrtu(String alamatOrtu) {
        this.alamatOrtu = alamatOrtu;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getFotoIjazah() {
        return fotoIjazah;
    }

    public void setFotoIjazah(String fotoIjazah) {
        this.fotoIjazah = fotoIjazah;
    }

    public String getFotoSkhun() {
        return fotoSkhun;
    }

    public void setFotoSkhun(String fotoSkhun) {
        this.fotoSkhun = fotoSkhun;
    }

    public String getAkte() {
        return akte;
    }

    public void setAkte(String akte) {
        this.akte = akte;
    }

    public String getKartuKeluarga() {
        return kartuKeluarga;
    }

    public void setKartuKeluarga(String kartuKeluarga) {
        this.kartuKeluarga = kartuKeluarga;
    }
    @Override
    public String toString() {
        return "DetailPendaftar{" +
                "pasFoto='" + getPasFoto() + '\'' +
                ", sekolahAsal='" + getSekolahAsal() + '\'' +
                ", namaCalon='" + getNamaCalon() + '\'' +
                ", idCalon='" + getIdCalon() + '\'' +
                ", statusPendaftaran='" + getStatusPendaftaran() + '\'' +
                ", kartuKeluarga='" + kartuKeluarga + '\'' +
                ", akte='" + akte + '\'' +
                ", fotoSkhun='" + fotoSkhun + '\'' +
                ", fotoIjazah='" + fotoIjazah + '\'' +
                ", noHp='" + noHp + '\'' +
                ", alamatOrtu='" + alamatOrtu + '\'' +
                ", pendidikanIbu='" + pendidikanIbu + '\'' +
                ", pendidikanAyah='" + pendidikanAyah + '\'' +
                ", pekerjaanIbu='" + pekerjaanIbu + '\'' +
                ", pekerjaanAyah='" + pekerjaanAyah + '\'' +
                ", namaIbu='" + namaIbu + '\'' +
                ", namaAyah='" + namaAyah + '\'' +
                ", jumlahSaudara='" + jumlahSaudara + '\'' +
                ", anak='" + anak + '\'' +
                ", alamat='" + alamat + '\'' +
                ", tahunIjazah='" + tahunIjazah + '\'' +
                ", agama='" + agama + '\'' +
                ", ttl='" + ttl + '\'' +
                ", jenis_kelamin='" + jenis_kelamin + '\'' +
                ", jenisPendaftaran='" + jenisPendaftaran + '\'' +
                ", email='" + getEmail() + '\'' +
                '}';
    }
}
