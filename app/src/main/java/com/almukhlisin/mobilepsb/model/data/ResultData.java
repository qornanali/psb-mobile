package com.almukhlisin.mobilepsb.model.data;

import java.io.Serializable;

public class ResultData<T> extends Result{

    private T data;
    private int size;


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "ResultData{" +
                "query='" + getQuery() + '\'' +
                ", message='" + getMessage() + '\'' +
                "size=" + size +
                ", data=" + data +
                '}';
    }
}
