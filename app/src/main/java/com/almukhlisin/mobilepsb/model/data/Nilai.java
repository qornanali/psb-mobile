package com.almukhlisin.mobilepsb.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by qornanali on 7/29/17.
 */

public class Nilai {

    @SerializedName("id_nilai")
    private String idNilai;
    @SerializedName("id_calon")
    private String idCalon;
    @SerializedName("nama_calon")
    private String namaCalon;
    @SerializedName("jenis_pendaftaran")
    private String jenisPendaftaran;
    @SerializedName("nilai_tulis")
    private String nilaiTulis;
    @SerializedName("nilai_baca")
    private String nilaiBaca;
    @SerializedName("pas_foto")
    private String pasFoto;
    @SerializedName("status_kelulusan")
    private String statusKelulusan;

    public String getNilaiBaca() {
        return nilaiBaca;
    }

    public void setNilaiBaca(String nilaiBaca) {
        this.nilaiBaca = nilaiBaca;
    }

    public String getIdNilai() {
        return idNilai;
    }

    public void setIdNilai(String idNilai) {
        this.idNilai = idNilai;
    }

    public String getIdCalon() {
        return idCalon;
    }

    public void setIdCalon(String idCalon) {
        this.idCalon = idCalon;
    }

    public String getNamaCalon() {
        return namaCalon;
    }

    public void setNamaCalon(String namaCalon) {
        this.namaCalon = namaCalon;
    }

    public String getJenisPendaftaran() {
        return jenisPendaftaran;
    }

    public void setJenisPendaftaran(String jenisPendaftaran) {
        this.jenisPendaftaran = jenisPendaftaran;
    }

    public String getNilaiTulis() {
        return nilaiTulis;
    }

    public void setNilaiTulis(String nilaiTulis) {
        this.nilaiTulis = nilaiTulis;
    }

    public String getPasFoto() {
        return pasFoto;
    }

    public void setPasFoto(String pasFoto) {
        this.pasFoto = pasFoto;
    }

    public String getStatusKelulusan() {
        return statusKelulusan;
    }

    public void setStatusKelulusan(String statusKelulusan) {
        this.statusKelulusan = statusKelulusan;
    }

    @Override
    public String toString() {
        return "Nilai{" +
                "statusKelulusan='" + statusKelulusan + '\'' +
                ", pasFoto='" + pasFoto + '\'' +
                ", nilaiBaca='" + nilaiBaca + '\'' +
                ", nilaiTulis='" + nilaiTulis + '\'' +
                ", jenisPendaftaran='" + jenisPendaftaran + '\'' +
                ", namaCalon='" + namaCalon + '\'' +
                ", idCalon='" + idCalon + '\'' +
                ", idNilai='" + idNilai + '\'' +
                '}';
    }
}
