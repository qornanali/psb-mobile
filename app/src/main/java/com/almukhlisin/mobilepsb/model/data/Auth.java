package com.almukhlisin.mobilepsb.model.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Auth {

    private String username;
    private String level;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "Auth{" +
                "username='" + username + '\'' +
                ", level='" + level + '\'' +
                '}';
    }
}
