package com.almukhlisin.mobilepsb.model.data;

public class Result {

    private String query;
    private String message;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Result{" +
                "query='" + query + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
