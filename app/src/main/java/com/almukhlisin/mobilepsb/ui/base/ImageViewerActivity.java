package com.almukhlisin.mobilepsb.ui.base;

import android.os.Bundle;

import com.almukhlisin.mobilepsb.BuildConfig;
import com.almukhlisin.mobilepsb.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import butterknife.Bind;


public class ImageViewerActivity extends BaseActivity {

    @Bind(R.id.photo_view)
    PhotoView photoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);

        setActionBarTitle(getMyBundle().getString("desc"));
        displayHome();

        Picasso.with(getMyContext())
                .load(BuildConfig.BASE_URL+getMyBundle().getString("url"))
                .error(R.color.gray)
                .into(photoView);
    }
}
