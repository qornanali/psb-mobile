package com.almukhlisin.mobilepsb.ui.laporan;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.model.data.Nilai;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.model.data.Tren;
import com.almukhlisin.mobilepsb.model.other.Laporan;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.util.adapter.ListLaporanAdapter;
import com.almukhlisin.mobilepsb.util.adapter.ListTrenAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import retrofit2.Call;

/**
 * Created by qornanali on 8/1/17.
 */

public class LihatTrenActivity extends BaseActivity {

    @Bind(R.id.rv_tren)
    RecyclerView rvTren;

    List<Tren> trenList;
    ListTrenAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tren_psb);
        initView();
        loadData();
    }


    private void initView(){
        trenList = new ArrayList<>();
        adapter = new ListTrenAdapter(getMyContext(), trenList) {
            @Override
            public void onItemClick(int position) {
            }
        };
        rvTren.setLayoutManager(new GridLayoutManager(getMyContext(), 2));
        rvTren.setAdapter(adapter);
    }

    private void loadData(){
        showProgressDialog(getString(R.string.message_loading_data), false);
        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<ResultData<List<Tren>>> request =
                restApi.getListTren();
        request.enqueue(new ApiCallback<ResultData<List<Tren>>>() {
            @Override
            public void onSuccess(ResultData<List<Tren>> data) {
                dismissProgressDialog();
                for (int i = 0; i < data.getData().size(); i++) {
                    trenList.add(data.getData().get(i));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }
}


