package com.almukhlisin.mobilepsb.ui.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.ui.akun.ListAkunActivity;
import com.almukhlisin.mobilepsb.ui.daftar_ulang.ListDaftarUlangActivity;
import com.almukhlisin.mobilepsb.ui.kuota.ListKuotaActivity;
import com.almukhlisin.mobilepsb.ui.laporan.LihatLaporanActivity;
import com.almukhlisin.mobilepsb.ui.laporan.LihatTrenActivity;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.DetailPendaftar;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.model.other.HomeMenu;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.ui.jadwal_ujian.ListJadwalUjianActivity;
import com.almukhlisin.mobilepsb.ui.login.LoginActivity;
import com.almukhlisin.mobilepsb.ui.nilai.ListNilaiActivity;
import com.almukhlisin.mobilepsb.ui.pendaftaran.ListPendaftarActivity;
import com.almukhlisin.mobilepsb.ui.pendaftaran.RegisterPendaftar;
import com.almukhlisin.mobilepsb.ui.pendaftaran.UploadAttachmentPendaftarActivity;
import com.almukhlisin.mobilepsb.ui.pengumuman.LihatPengumumanActivity;
import com.almukhlisin.mobilepsb.ui.pengumuman.UpdatePengumumanActivity;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.adapter.ListHomeMenuAdapter;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

public class DashboardActivity extends BaseActivity {

    @Bind(R.id.rv_home_menu)
    RecyclerView rvHomeMenu;
    @Bind(R.id.tv_dashboard_user_level)
    TextView tvUserLevel;
    @Bind(R.id.tv_dashboard_user_name)
    TextView tvUserName;
    @Bind(R.id.cv_user_profile)
    CardView cvUserProfile;

    List<HomeMenu> homeMenus;
    ListHomeMenuAdapter adapter;
    Auth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        auth = Hawk.get("auth");
        homeMenus = new ArrayList<>();
        loadHomeMenus();
    }

    private void loadHomeMenus(){
        adapter = new ListHomeMenuAdapter(getMyContext(), homeMenus) {
            @Override
            public void onItemClick(int position) {
                switch (homeMenus.get(position).getId()){
                    case 1 :
                        goToActivity(LihatTrenActivity.class, null, false);
                        break;
                    case 2 :
                        goToActivity(RegisterPendaftar.class, null, false);
                        break;
                    case 3 :
                        goToActivityForResult(LoginActivity.class, null, 1);
                        break;
                    case 4 :
                        Bundle bundle4 = new Bundle();
                        bundle4.putString("level", auth.getLevel());
                        goToActivity(ListPendaftarActivity.class, bundle4, false);
                        break;
                    case 5 :
                        goToActivity(ListJadwalUjianActivity.class, null, false);
                        break;
                    case 6 :
                        goToActivity(ListNilaiActivity.class, null, false);
                        break;
                    case 7 :
                        goToActivity((auth.getLevel().equals("Pendaftar") ?
                                LihatPengumumanActivity.class :
                                UpdatePengumumanActivity.class), null, false);
                        break;
                    case 8 :
                        goToActivity(ListDaftarUlangActivity.class, null, false);
                        break;
                    case 9 :
                        goToActivity(ListAkunActivity.class, null, false);
                        break;
                    case 10 :
                        Bundle bundle10 = new Bundle();
                        bundle10.putString("username", auth.getUsername());
                        goToActivity(UploadAttachmentPendaftarActivity.class, bundle10, false);
                        break;
                    case 11 :
                        Bundle bundle11 = new Bundle();
                        bundle11.putString("username", auth.getUsername());
                        goToActivity(RegisterPendaftar.class, bundle11, false);
                        break;
                    case 12 :
                        goToActivity(LihatLaporanActivity.class, null, false);
                        break;
                    case 13 :
                        goToActivity(ListKuotaActivity.class, null, false);
                        break;
                }
            }
        };
        rvHomeMenu.setLayoutManager(new LinearLayoutManager(getMyContext()));
        rvHomeMenu.setAdapter(adapter);
        rvHomeMenu.scrollToPosition(0);
        loadLevelMenu();
        loadUserProfile();
    }

    private void loadLevelMenu(){
        homeMenus.clear();
        if(auth == null){
//            homeMenus.add(new HomeMenu(getString(R.string.menu_support_psb), 0));
            homeMenus.add(new HomeMenu(getString(R.string.menu_trend), 1));
            homeMenus.add(new HomeMenu(getString(R.string.menu_register), 2));
            homeMenus.add(new HomeMenu(getString(R.string.menu_login), 3));
        }else if(auth.getLevel().equals("Administrator")){
            homeMenus.add(new HomeMenu(getString(R.string.menu_participant), 4));
            homeMenus.add(new HomeMenu(getString(R.string.menu_exam), 5));
            homeMenus.add(new HomeMenu(getString(R.string.menu_score), 6));
            homeMenus.add(new HomeMenu(getString(R.string.menu_announcement), 7));
            homeMenus.add(new HomeMenu(getString(R.string.menu_reregistration), 8));
            homeMenus.add(new HomeMenu(getString(R.string.menu_manage_admin), 9));
            homeMenus.add(new HomeMenu(getString(R.string.menu_manage_kuota), 13));
        }else if(auth.getLevel().equals("Pendaftar")){
            homeMenus.add(new HomeMenu(getString(R.string.menu_participant), 4));
            homeMenus.add(new HomeMenu(getString(R.string.menu_exam), 5));
            homeMenus.add(new HomeMenu(getString(R.string.menu_score), 6));
            homeMenus.add(new HomeMenu(getString(R.string.menu_announcement), 7));
            homeMenus.add(new HomeMenu(getString(R.string.menu_upload_file), 10));
            homeMenus.add(new HomeMenu(getString(R.string.menu_change_profile), 11));
        }else{
            homeMenus.add(new HomeMenu(getString(R.string.menu_trend), 1));
            homeMenus.add(new HomeMenu(getString(R.string.menu_report), 12));
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            auth = Hawk.get("auth");
            loadLevelMenu();
            loadUserProfile();
        }
    }

    private void loadUserProfile(){
        if(auth == null){
            cvUserProfile.setVisibility(View.GONE);
        }else{
            if(auth.getLevel().equals("Pendaftar")){
                showProgressDialog(getString(R.string.message_loading_data), false);
                RestApi restApi = RetrofitBuilder.create(RestApi.class);
                Call<ResultData<List<DetailPendaftar>>> resultDataCall
                        = restApi.getListDetailPendaftar(
                        null, null, null, null, auth.getUsername(), null
                );
                resultDataCall.enqueue(new ApiCallback<ResultData<List<DetailPendaftar>>>() {
                    @Override
                    public void onSuccess(ResultData<List<DetailPendaftar>> data) {
                        dismissProgressDialog();
                        if(data.getData().size() > 0){
                            Hawk.put("detail_pendaftar", data.getData().get(0));
                        }
                    }

                    @Override
                    public void onFailed(Throwable t) {
                        dismissProgressDialog();
                        Logger.log(Log.ERROR, t.getMessage());
                    }
                });
            }
            cvUserProfile.setVisibility(View.VISIBLE);
            tvUserLevel.setText(getString(R.string.field_level) + " : " + auth.getLevel());
            tvUserName.setText(getString(R.string.field_username) + " : " + auth.getUsername());
        }
    }

    @OnClick(R.id.btn_dashboard_log_out)
    public void onBtnLogoutClick(){
        showProgressDialog(getString(R.string.message_loging_out), false);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Hawk.deleteAll();
                auth = Hawk.get("auth");
                loadLevelMenu();
                loadUserProfile();
                dismissProgressDialog();
            }
        }, 1000);
    }
}
