package com.almukhlisin.mobilepsb.ui.pendaftaran;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.almukhlisin.mobilepsb.BuildConfig;
import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.DetailPendaftar;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.model.other.Attachment;
import com.almukhlisin.mobilepsb.model.other.DataDetailPendaftar;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.ui.base.ImageViewerActivity;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.adapter.ListAttachmentAdapter;
import com.almukhlisin.mobilepsb.util.adapter.ListDataDetailPendaftarAdapter;
import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

public class DetailPendaftarActivity extends BaseActivity {

    @Bind(R.id.rv_data_detail_pendaftar)
    RecyclerView rvDataDetailPendaftar;
    @Bind(R.id.rv_attachment_detail_pendaftar)
    RecyclerView rvAttachmentDetailPendaftar;
    @Bind(R.id.iv_detail_pendaftar_pas_foto)
    ImageView ivDataDetailPendaftarPasFoto;
    @Bind(R.id.tv_detail_pendaftar_id_calon)
    TextView tvDetailPendaftarIdCalon;
    @Bind(R.id.tv_detail_pendaftar_nama)
    TextView tvDetailPendaftarNama;
    @Bind(R.id.tv_detail_status_pendaftar)
    TextView tvDetailStatusPendaftar;
    @Bind(R.id.btn_detail_pendaftar_verify)
    Button btnDetailVerify;

    DetailPendaftar detailPendaftar;
    RestApi restApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pendaftar);
        setActionBarTitle(getString(R.string.detail_pendaftar));
        displayHome();
        loadData();
    }

    private void loadData(){
        restApi = RetrofitBuilder.create(RestApi.class);
        if(getMyBundle() != null){
            showProgressDialog(getString(R.string.message_loading_data), false);
            Call<ResultData<List<DetailPendaftar>>> resultDataCall
                    = restApi.getListDetailPendaftar(
                    null, null, null, null, getMyBundle().getString("username"), null
            );
            resultDataCall.enqueue(new ApiCallback<ResultData<List<DetailPendaftar>>>() {
                @Override
                public void onSuccess(ResultData<List<DetailPendaftar>> data) {
                    dismissProgressDialog();
                    if(data.getData().size() > 0){
                        detailPendaftar = data.getData().get(0);
                        implementView();
                    }
                }

                @Override
                public void onFailed(Throwable t) {
                    dismissProgressDialog();
                    Logger.log(Log.ERROR, t.getMessage());
                }
            });
        }
    }

    private void implementView(){
        Picasso.with(getMyContext())
                .load(BuildConfig.BASE_URL+detailPendaftar.getPasFoto())
                .resize(140, 140)
                .error(R.color.gray)
                .into(ivDataDetailPendaftarPasFoto);
        ivDataDetailPendaftarPasFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("url", detailPendaftar.getPasFoto());
                bundle.putString("desc", getString(R.string.field_pas_photo));
                goToActivity(ImageViewerActivity.class, bundle, false);
            }
        });
        tvDetailPendaftarIdCalon.setText(detailPendaftar.getIdCalon());
        tvDetailStatusPendaftar.setText(detailPendaftar.getStatusPendaftaran());
        tvDetailPendaftarNama.setText(detailPendaftar.getNamaCalon());
        tvDetailStatusPendaftar.setTextColor(
                getResources().getColor(
                        detailPendaftar.getStatusPendaftaran().equals("Sudah Dikonfirmasi")
                                ? R.color.green : R.color.red));
        List<DataDetailPendaftar> dataDetailPendaftarList = new ArrayList<>();
        dataDetailPendaftarList.add(
                new DataDetailPendaftar(detailPendaftar.getJenis_kelamin(), getString(R.string.field_gender)));
        dataDetailPendaftarList.add(
                new DataDetailPendaftar(detailPendaftar.getTtl(), getString(R.string.field_birth_place) + " & " + getString(R.string.field_birth_date)));
        dataDetailPendaftarList.add(
                new DataDetailPendaftar(detailPendaftar.getEmail(), getString(R.string.field_email)));
        dataDetailPendaftarList.add(
                new DataDetailPendaftar(detailPendaftar.getAlamat(), getString(R.string.field_address)));
        dataDetailPendaftarList.add(new
                DataDetailPendaftar(detailPendaftar.getNoHp(), getString(R.string.field_phone_number)));
        dataDetailPendaftarList.add(new
                DataDetailPendaftar(detailPendaftar.getSekolahAsal() + " " + detailPendaftar.getTahunIjazah(),
                getString(R.string.field_school) + " dan " + getString(R.string.field_graduate_year)));
        dataDetailPendaftarList.add(new
                DataDetailPendaftar(detailPendaftar.getJenisPendaftaran(), getString(R.string.field_register_type)));
        dataDetailPendaftarList.add(new
                DataDetailPendaftar(detailPendaftar.getAgama(), getString(R.string.field_religion)));
        dataDetailPendaftarList.add(new
                DataDetailPendaftar(detailPendaftar.getNamaAyah(), getString(R.string.field_dad_name)));
        dataDetailPendaftarList.add(new
                DataDetailPendaftar(detailPendaftar.getPekerjaanAyah(), getString(R.string.field_dad_job)));
        dataDetailPendaftarList.add(new
                DataDetailPendaftar(detailPendaftar.getPendidikanAyah(), getString(R.string.field_dad_school)));
        dataDetailPendaftarList.add(new
                DataDetailPendaftar(detailPendaftar.getNamaIbu(), getString(R.string.field_mom_name)));
        dataDetailPendaftarList.add(new
                DataDetailPendaftar(detailPendaftar.getPekerjaanIbu(), getString(R.string.field_mom_job)));
        dataDetailPendaftarList.add(new
                DataDetailPendaftar(detailPendaftar.getPendidikanIbu(), getString(R.string.field_mom_school)));
        dataDetailPendaftarList.add(new
                DataDetailPendaftar( detailPendaftar.getAlamatOrtu(), getString(R.string.field_parent_address)));
        dataDetailPendaftarList.add(new
                DataDetailPendaftar(detailPendaftar.getAnak(), getString(R.string.field_child)+" ke"));
        dataDetailPendaftarList.add(new
                DataDetailPendaftar(detailPendaftar.getJumlahSaudara(), getString(R.string.field_brosis)));
        ListDataDetailPendaftarAdapter dataDetailPendaftarAdapter =
                new ListDataDetailPendaftarAdapter(getMyContext(), dataDetailPendaftarList);
        rvDataDetailPendaftar.setLayoutManager(new LinearLayoutManager(getMyContext()));
        rvDataDetailPendaftar.setAdapter(dataDetailPendaftarAdapter);

        final List<Attachment> attachments = new ArrayList<>();
        attachments.add(new Attachment(getString(R.string.field_skhun), detailPendaftar.getFotoSkhun()));
        attachments.add(new Attachment(getString(R.string.field_akte), detailPendaftar.getAkte()));
        attachments.add(new Attachment(getString(R.string.field_ijazah), detailPendaftar.getFotoIjazah()));
        attachments.add(new Attachment(getString(R.string.field_kk), detailPendaftar.getKartuKeluarga()));
        ListAttachmentAdapter attachmentAdapter =
                new ListAttachmentAdapter(getMyContext(), attachments) {
                    @Override
                    public void onItemClick(int position) {
                        Bundle bundle = new Bundle();
                        bundle.putString("url", attachments.get(position).getUrl());
                        bundle.putString("desc", attachments.get(position).getDescription());
                        goToActivity(ImageViewerActivity.class, bundle, false);
                    }
                };
        rvAttachmentDetailPendaftar.setLayoutManager(new GridLayoutManager(getMyContext(), 2));
        rvAttachmentDetailPendaftar.setAdapter(attachmentAdapter);

        Auth auth = Hawk.get("auth");
        if(!auth.getLevel().equals("Administrator") || detailPendaftar.getStatusPendaftaran().equals("Sudah Dikonfirmasi")){
            btnDetailVerify.setVisibility(View.GONE);
        }else{
            btnDetailVerify.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_detail_pendaftar_verify)
    public void onBtnDetailVerify(){
        showProgressDialog(getString(R.string.message_confirming), false);
        Call<Result> request = restApi.postVerifyPendaftar(detailPendaftar.getIdCalon());
        request.enqueue(new ApiCallback<Result>() {
            @Override
            public void onSuccess(Result data) {
                dismissProgressDialog();
                loadData();
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
            }
        });
    }
}
