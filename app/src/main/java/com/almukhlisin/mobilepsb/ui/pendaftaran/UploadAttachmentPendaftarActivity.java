package com.almukhlisin.mobilepsb.ui.pendaftaran;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.DetailPendaftar;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.model.other.Attachment;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.adapter.ListUploadAttachmentAdapter;
import com.orhanobut.hawk.Hawk;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * Created by qornanali on 7/27/17.
 */

public class UploadAttachmentPendaftarActivity extends BaseActivity {
    
    @Bind(R.id.rv_upload_attachment)
    RecyclerView rvUploadAttachment;

    ListUploadAttachmentAdapter attachmentAdapter;
    List<Attachment> attachmentList;
    DetailPendaftar detailPendaftar;

    RestApi restApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_attachment);
        setActionBarTitle(getString(R.string.menu_upload_file));
        implementView();
        displayHome();
        loadData();
    }

    private void loadData(){
        attachmentList.add(new Attachment
                (getString(R.string.field_pas_photo), "", "pas_foto"));
        attachmentList.add(new Attachment
                (getString(R.string.field_skhun), "", "foto_skhun"));
        attachmentList.add(new Attachment
                (getString(R.string.field_akte), "", "akte"));
        attachmentList.add(new Attachment
                (getString(R.string.field_ijazah), "", "foto_ijazah"));
        attachmentList.add(new Attachment
                (getString(R.string.field_kk), "", "kartu_keluarga"));
        attachmentAdapter.notifyDataSetChanged();
        restApi = RetrofitBuilder.create(RestApi.class);
        detailPendaftar = Hawk.get("detail_pendaftar");
        if(detailPendaftar != null){
            Hawk.put("id_calon", detailPendaftar.getIdCalon());
            String[] url = {detailPendaftar.getPasFoto(),
                    detailPendaftar.getFotoSkhun(), detailPendaftar.getAkte(),
                    detailPendaftar.getFotoIjazah(), detailPendaftar.getKartuKeluarga()};
            for (int i = 0; i < url.length; i++) {
                attachmentList.get(i).setUrl(url[i]);
                attachmentAdapter.notifyItemChanged(i);
            }
        }
    }

    private void implementView(){
        attachmentList = new ArrayList<>();
        attachmentAdapter =
                new ListUploadAttachmentAdapter(getMyContext(), attachmentList) {
                    @Override
                    public void onItemClick(int position) {
                        Hawk.put("type", attachmentList.get(position).getId());
                        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                        getIntent.setType("image/*");

                        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        pickIntent.setType("image/*");

                        Intent chooserIntent = Intent.createChooser(getIntent, getString(R.string.message_select_image));
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

                        startActivityForResult(chooserIntent, position+10);
                    }
                };
        rvUploadAttachment.setLayoutManager(new GridLayoutManager(getMyContext(), 2));
        rvUploadAttachment.setAdapter(attachmentAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode >= 10 && resultCode == RESULT_OK){
            if (data == null) {
                return;
            }
            try{
                Uri uri = data.getData();
                String realPath = "";
                Cursor cursor = null;
                try {
                    String[] proj = { MediaStore.Images.Media.DATA };
                    cursor = getMyContext().getContentResolver().query(uri,  proj, null, null, null);
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    realPath = cursor.getString(column_index);
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
                File file = new File(realPath);

                MultipartBody.Builder builder = new MultipartBody.Builder();
                builder.setType(MultipartBody.FORM);
                builder.addFormDataPart("image",file.getName(),RequestBody.create(MediaType.parse("image/*"), file));

                attachmentList.get(requestCode - 10).setUrl("");
                attachmentList.get(requestCode - 10).setFile(realPath);
                attachmentAdapter.notifyItemChanged(requestCode - 10);

                MultipartBody requestBody = builder.build();
                String id = (String) Hawk.get("id_calon");
                String type = (String) Hawk.get("type");

                showProgressDialog(getString(R.string.message_uploading), false);
                Call<Result> service = restApi.postUploadImage(
                        id, type, requestBody
                );
                service.enqueue(new ApiCallback<Result>() {
                    @Override
                    public void onSuccess(Result data) {
                        dismissProgressDialog();
                        showSnackBar(data.getMessage());
                        Hawk.delete("type");
                    }

                    @Override
                    public void onFailed(Throwable t) {
                        dismissProgressDialog();
                        Logger.log(Log.ERROR, t.getMessage());
                    }
                });
            }catch (Exception e){
                dismissProgressDialog();
                Logger.log(Log.ERROR, e.getMessage());
            }
        }
    }
}
