package com.almukhlisin.mobilepsb.ui.login;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.Logger;
import com.orhanobut.hawk.Hawk;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

public class LoginActivity extends BaseActivity {

    @Bind(R.id.et_login_password)
    EditText etLoginPassword;
    @Bind(R.id.et_login_username)
    EditText etLoginUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setActionBarTitle(getString(R.string.title_login));
        displayHome();
    }

    @OnClick(R.id.btn_login_do_login)
    public void onBtnDoLoginClick(){
        String username = etLoginUsername.getText().toString();
        String password = etLoginPassword.getText().toString();

        if(username.isEmpty() || password.isEmpty()){
            showSnackBar(getString(R.string.message_field_empty));
            return;
        }

        showProgressDialog(getString(R.string.message_loging_in, false));

        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<ResultData<List<Auth>>> request = restApi.postLogin(
                username, password
        );
        request.enqueue(new ApiCallback<ResultData<List<Auth>>>() {
            @Override
            public void onSuccess(ResultData<List<Auth>> data) {
                dismissProgressDialog();
                if(data.getData().size() > 0){
                    Auth auth = data.getData().get(0);
                    Hawk.put("auth", auth);
                    showSnackBar(data.getMessage());
                    finishActivityforResult(null, 1);
                }else{
                    showSnackBar(getString(R.string.message_wrong_login));
                }
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }

}
