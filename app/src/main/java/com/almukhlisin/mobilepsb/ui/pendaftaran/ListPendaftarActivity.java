package com.almukhlisin.mobilepsb.ui.pendaftaran;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.Pendaftar;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.adapter.ListPendaftarAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import retrofit2.Call;

public class ListPendaftarActivity extends BaseActivity {

    @Bind(R.id.rv_list_pendaftar)
    RecyclerView rvListPendaftar;

    List<Pendaftar> pendaftarList;
    ListPendaftarAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_pendaftar);
        setActionBarTitle(getString(R.string.menu_participant));
        displayHome();
        initView();
        loadData();
    }

    private void initView(){
        pendaftarList = new ArrayList<>();
        adapter = new ListPendaftarAdapter(getMyContext(), pendaftarList) {
            @Override
            public void onItemClick(int position) {
                Bundle bundle = new Bundle();
                bundle.putString("username", pendaftarList.get(position).getEmail());
                goToActivity(DetailPendaftarActivity.class, bundle, false);
            }
        };
        rvListPendaftar.setLayoutManager(new LinearLayoutManager(getMyContext()));
        rvListPendaftar.setAdapter(adapter);
    }

    private void loadData(){
        showProgressDialog(getString(R.string.message_loading_data), false);
        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<ResultData<List<Pendaftar>>> request = null;
        if(getMyBundle().getString("level").equals("Pendaftar")){
            request = restApi.getListPendaftar(
                    null, null, null, "true", null, null
            );
        }else{
            request = restApi.getListPendaftar(
                    null, null, null, null, null, null
            );
        }
        request.enqueue(new ApiCallback<ResultData<List<Pendaftar>>>() {
            @Override
            public void onSuccess(ResultData<List<Pendaftar>> data) {
                dismissProgressDialog();
                for (int i = 0; i < data.getData().size(); i++) {
                    pendaftarList.add(data.getData().get(i));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }
}
