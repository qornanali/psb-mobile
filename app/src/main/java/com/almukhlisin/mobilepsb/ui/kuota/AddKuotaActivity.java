package com.almukhlisin.mobilepsb.ui.kuota;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.model.data.Akun;
import com.almukhlisin.mobilepsb.model.data.Kuota;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * Created by qornanali on 7/29/17.
 */

public class AddKuotaActivity extends BaseActivity {

    @Bind(R.id.et_jumlah)
    EditText etJumlah;
    @Bind(R.id.et_tahun)
    EditText etTahun;
    @Bind(R.id.spv_register_type)
    Spinner spvRegisterType;

    Kuota kuota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_kuota);
        setActionBarTitle(getString(R.string.do_add_kuota));
        kuota = new Kuota();
        displayHome();
        initView();
    }

    private void initView(){
        String[] jenisPendaftaran = {"MI", "MTS", "MA"};

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, jenisPendaftaran);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spvRegisterType.setAdapter(spinnerArrayAdapter);
    }

    @OnClick(R.id.btn_do_add_akun)
    public void onDoAddClick(){
        insertData();
        showProgressDialog(getString(R.string.message_adding_akun), false);
        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<Result> request = restApi.postInsertKuota(
          kuota.getJenisPendaftaran(), kuota.getTahun(),
                kuota.getJumlah()
        );
        request.enqueue(new ApiCallback<Result>() {
            @Override
            public void onSuccess(Result data) {
                dismissProgressDialog();
                showSnackBar(data.getMessage());
                if(data.getMessage().equals("sukses")){
                    onBackPressed();
                }
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }

    private void insertData(){
        kuota.setJumlah(etJumlah.getText().toString());
        kuota.setJenisPendaftaran((String) spvRegisterType.getSelectedItem());
        kuota.setTahun(etTahun.getText().toString());
    }
}
