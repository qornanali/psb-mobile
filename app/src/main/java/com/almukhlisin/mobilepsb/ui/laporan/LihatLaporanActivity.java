package com.almukhlisin.mobilepsb.ui.laporan;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.model.data.Nilai;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.model.other.Laporan;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.util.adapter.ListLaporanAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import retrofit2.Call;

/**
 * Created by qornanali on 8/1/17.
 */

public class LihatLaporanActivity extends BaseActivity {

    @Bind(R.id.rv_laporan)
    RecyclerView rvLaporan;

    List<Laporan> laporanList;
    ListLaporanAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_laporan);
        initView();
        loadData();
    }


    private void initView(){
        laporanList = new ArrayList<>();
        adapter = new ListLaporanAdapter(getMyContext(), laporanList) {
            @Override
            public void onItemClick(int position) {
            }
        };
        rvLaporan.setLayoutManager(new LinearLayoutManager(getMyContext()));
        rvLaporan.setAdapter(adapter);
    }

    private void loadData(){
        laporanList.add(new Laporan(
                getString(R.string.field_full_name), "",
                getString(R.string.field_nilai_baca), getString(R.string.field_nilai_tulis)
        ));
        showProgressDialog(getString(R.string.message_loading_data), false);
        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<ResultData<List<Nilai>>> request =
                restApi.getListNilai(
                        null, null, null, null, "true"
                );
        request.enqueue(new ApiCallback<ResultData<List<Nilai>>>() {
            @Override
            public void onSuccess(ResultData<List<Nilai>> data) {
                dismissProgressDialog();
                for(Nilai item : data.getData()){
                    laporanList.add(new Laporan(
                            item.getNamaCalon(), item.getJenisPendaftaran(),
                            item.getNilaiBaca(), item.getNilaiTulis()
                    ));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }
}


