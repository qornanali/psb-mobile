package com.almukhlisin.mobilepsb.ui.akun;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.model.data.Akun;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.DetailPendaftar;
import com.almukhlisin.mobilepsb.model.data.Nilai;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.util.adapter.ListAkunAdapter;
import com.almukhlisin.mobilepsb.util.adapter.ListNilaiAdapter;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

public class ListAkunActivity extends BaseActivity {

    @Bind(R.id.rv_list_akun)
    RecyclerView rvListAkun;

    List<Akun> akunList;
    ListAkunAdapter adapter;
    Auth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_akun);
        setActionBarTitle(getString(R.string.menu_manage_admin));
        displayHome();
        initView();
        loadData();
    }

    private void initView(){
        akunList = new ArrayList<>();
        adapter = new ListAkunAdapter(getMyContext(), akunList) {
            @Override
            public void onItemClick(int position) {
            }
        };
        rvListAkun.setLayoutManager(new LinearLayoutManager(getMyContext()));
        rvListAkun.setAdapter(adapter);
    }

    @OnClick(R.id.btn_do_add_akun)
    public void onBtnDoAddAkunClick(){
        goToActivity(AddAkunActivity.class, null, false);
    }

    private void loadData(){
        showProgressDialog(getString(R.string.message_loading_data), false);
        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<ResultData<List<Akun>>> request =
                restApi.getListAkun(
                    null
            );
        request.enqueue(new ApiCallback<ResultData<List<Akun>>>() {
            @Override
            public void onSuccess(ResultData<List<Akun>> data) {
                dismissProgressDialog();
                for (int i = 0; i < data.getData().size(); i++) {
                    akunList.add(data.getData().get(i));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }
}
