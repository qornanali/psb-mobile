package com.almukhlisin.mobilepsb.ui.pengumuman;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.almukhlisin.mobilepsb.BuildConfig;
import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.Daful;
import com.almukhlisin.mobilepsb.model.data.DetailPendaftar;
import com.almukhlisin.mobilepsb.model.data.Nilai;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.ui.base.ImageViewerActivity;
import com.almukhlisin.mobilepsb.util.Logger;
import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * Created by qornanali on 7/29/17.
 */

public class LihatPengumumanActivity extends BaseActivity {

    @Bind(R.id.tv_pengumuman_daful)
    TextView tvPengumumanDaful;
    @Bind(R.id.tv_pengumuman_footer)
    TextView tvPengumumanFooter;
    @Bind(R.id.tv_pengumuman_header)
    TextView tvPengumumanHeader;
    @Bind(R.id.tv_pengumuman_result)
    TextView tvPengumumanResult;
    @Bind(R.id.tv_pengumuman_id_calon)
    TextView tvPengumumanIdCalon;
    @Bind(R.id.tv_pengumuman_nama_calon)
    TextView tvPengumumanNamaCalon;
    @Bind(R.id.tv_pengumuman_konfirmasi)
    TextView tvPengumumanKonfirmasi;
    @Bind(R.id.rl_attachment)
    RelativeLayout rlAttachment;
    @Bind(R.id.iv_pengumuman_pas_foto)
    ImageView ivPengumumanPasFoto;
    @Bind(R.id.iv_attachment)
    ImageView ivAttachment;
    @Bind(R.id.btn_do_upload)
    Button btnDoUpload;

    boolean tidaklulus = true;
    DetailPendaftar detailPendaftar;
    Daful daful;

    RestApi restApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_pengumuman);
        setActionBarTitle(getString(R.string.menu_announcement));
        restApi = RetrofitBuilder.create(RestApi.class);
        displayHome();
        detailPendaftar = Hawk.get("detail_pendaftar");
        loadData();
    }

    private void loadData(){
        showProgressDialog(getString(R.string.message_loading_data));
        if(detailPendaftar != null){
            Call<ResultData<List<Nilai>>> request = restApi.getListNilai(
                    null, null, null, detailPendaftar.getIdCalon(), null
            );
            request.enqueue(new ApiCallback<ResultData<List<Nilai>>>() {
                @Override
                public void onSuccess(ResultData<List<Nilai>> data) {
                    dismissProgressDialog();
                    if(data.getData().size()>0){
                        if(data.getData().get(0).getStatusKelulusan().equals("Diterima")){
                            tidaklulus = false;
                        }
                    }
                    implementView();
                }

                @Override
                public void onFailed(Throwable t) {
                    dismissProgressDialog();
                    Logger.log(Log.ERROR, t.getMessage());
                }
            });
        }
    }

    private void implementView(){
        if(!detailPendaftar.getPasFoto().equals("")){
            Picasso.with(getMyContext()).load(BuildConfig.BASE_URL+detailPendaftar.getPasFoto())
                    .error(R.color.gray)
                    .resize(120, 120).into(ivPengumumanPasFoto);
        }
        tvPengumumanIdCalon.setText(getString(R.string.id_calon) + " : " + detailPendaftar.getIdCalon());
        tvPengumumanNamaCalon.setText(getString(R.string.field_full_name) + " : " + detailPendaftar.getNamaCalon());
        tvPengumumanHeader.setText(tidaklulus ?
                getString(R.string.message_sorry) :
                getString(R.string.message_congratulations));
        tvPengumumanResult.setText(tidaklulus ?
                getString(R.string.message_not_accepted) :
                getString(R.string.message_accepted));
        tvPengumumanFooter.setText(getString(R.string.message_as_new) + " " +
                getString(R.string.al_mukhlisin) + "\n" + getString(R.string.message_periode));
        if(!tidaklulus){
            tvPengumumanDaful.setVisibility(View.VISIBLE);
            btnDoUpload.setVisibility(View.VISIBLE);
            tvPengumumanDaful.setText(getString(R.string.message_daful));
            loadStatusKonfirmasi();
        }
        tvPengumumanResult.setTextColor(
                getMyContext().getResources().getColor(
                        !tidaklulus ? R.color.green : R.color.red));
    }

    @OnClick(R.id.btn_do_upload)
    public void onBtnUploadClick(){
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, getString(R.string.message_select_image));
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, 1);
    }

    private void updateImage(Uri uri){
        try{
            String realPath = "";
            Cursor cursor = null;
            try {
                String[] proj = { MediaStore.Images.Media.DATA };
                cursor = getMyContext().getContentResolver().query(uri,  proj, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                realPath = cursor.getString(column_index);
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            File file = new File(realPath);

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("image",file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

            MultipartBody requestBody = builder.build();
            String id = detailPendaftar.getIdCalon();
            String type = "bukti";

            showProgressDialog(getString(R.string.message_uploading), false);
            Call<Result> service = restApi.postUploadImage(
                    id, type, requestBody
            );
            service.enqueue(new ApiCallback<Result>() {
                @Override
                public void onSuccess(Result data) {
                    dismissProgressDialog();
                    showSnackBar(data.getMessage());
                }

                @Override
                public void onFailed(Throwable t) {
                    dismissProgressDialog();
                    Logger.log(Log.ERROR, t.getMessage());
                }
            });
        }catch (Exception e){
            dismissProgressDialog();
            Logger.log(Log.ERROR, e.getMessage());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == RESULT_OK){
            if (data == null) {
                return;
            }
            showProgressDialog(getString(R.string.message_adding_daful));
            final Uri uri = data.getData();
            daful = Hawk.get("daful");
            if(daful != null){
                updateImage(uri);
            }else{
                Call<Result> request = restApi.postInsertDaftarUlang(detailPendaftar.getIdCalon());
                request.enqueue(new ApiCallback<Result>() {
                    @Override
                    public void onSuccess(Result data) {
                        dismissProgressDialog();
                        showSnackBar(data.getMessage());
                        if(data.getMessage().equals("sukses")){
                            updateImage(uri);
                        }
                    }

                    @Override
                    public void onFailed(Throwable t) {
                        dismissProgressDialog();
                        Logger.log(Log.ERROR, t.getMessage());
                    }
                });
            }
        }
    }

    private void loadStatusKonfirmasi() {
        Hawk.delete("daful");
        showProgressDialog(getString(R.string.message_loading_data));
        if (detailPendaftar != null) {
            RestApi restApi = RetrofitBuilder.create(RestApi.class);
            Call<ResultData<List<Daful>>> request = restApi.getListDaftarUlang(
                    null, null, null, detailPendaftar.getIdCalon(), null
            );
            request.enqueue(new ApiCallback<ResultData<List<Daful>>>() {
                @Override
                public void onSuccess(ResultData<List<Daful>> data) {
                    dismissProgressDialog();
                    rlAttachment.setVisibility(View.VISIBLE);
                    tvPengumumanKonfirmasi.setVisibility(View.VISIBLE);
                    String text = getString(R.string.message_not_upload);
                    if (data.getData().size() > 0) {
                        daful = data.getData().get(0);
                        if(!daful.getBukti().equals("")){
                            Picasso.with(getMyContext())
                                    .load(BuildConfig.BASE_URL+daful.getBukti())
                                    .into(ivAttachment);
                            ivAttachment.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("url", daful.getBukti());
                                    bundle.putString("desc", getString(R.string.field_proof));
                                    goToActivity(ImageViewerActivity.class, bundle, false);
                                }
                            });
                        }
                        text = daful.getStatus();
                        Hawk.put("daful", daful);
                    }
                    tvPengumumanKonfirmasi.setText(text);
                }

                @Override
                public void onFailed(Throwable t) {
                    dismissProgressDialog();
                    Logger.log(Log.ERROR, t.getMessage());
                }
            });
        }
    }

}
