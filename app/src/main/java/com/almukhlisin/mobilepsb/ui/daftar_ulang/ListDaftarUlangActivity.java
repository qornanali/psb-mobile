package com.almukhlisin.mobilepsb.ui.daftar_ulang;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.Daful;
import com.almukhlisin.mobilepsb.model.data.DetailPendaftar;
import com.almukhlisin.mobilepsb.model.data.Nilai;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.ui.nilai.AddNilaiActivity;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.util.adapter.ListDafulAdapter;
import com.almukhlisin.mobilepsb.util.adapter.ListNilaiAdapter;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

public class ListDaftarUlangActivity extends BaseActivity {

    @Bind(R.id.rv_list_daful)
    RecyclerView rvListDaful;

    List<Daful> dafulList;
    ListDafulAdapter adapter;
    Auth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_daful);
        setActionBarTitle(getString(R.string.menu_reregistration));
        displayHome();
        initView();
        loadData();
    }

    private void initView(){
        dafulList = new ArrayList<>();
        adapter = new ListDafulAdapter(getMyContext(), dafulList) {
            @Override
            public void onItemClick(int position) {
            }
        };
        rvListDaful.setLayoutManager(new LinearLayoutManager(getMyContext()));
        rvListDaful.setAdapter(adapter);
    }


    private void loadData(){
        auth = Hawk.get("auth");
        showProgressDialog(getString(R.string.message_loading_data), false);
        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<ResultData<List<Daful>>> request = null;
            request = restApi.getListDaftarUlang(
                    null, null, null, null, null
            );
        request.enqueue(new ApiCallback<ResultData<List<Daful>>>() {
            @Override
            public void onSuccess(ResultData<List<Daful>> data) {
                dismissProgressDialog();
                for (int i = 0; i < data.getData().size(); i++) {
                    dafulList.add(data.getData().get(i));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }
}
