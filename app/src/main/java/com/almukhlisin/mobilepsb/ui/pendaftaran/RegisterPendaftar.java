package com.almukhlisin.mobilepsb.ui.pendaftaran;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.DetailPendaftar;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.Logger;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

public class RegisterPendaftar extends BaseActivity {

    @Bind(R.id.et_register_full_name)
    EditText etRegisterFullname;
    @Bind(R.id.rb_register_man)
    RadioButton rbRegisterMan;
    @Bind(R.id.rb_register_woman)
    RadioButton rbRegisterWoman;
    @Bind(R.id.et_register_birth_place)
    EditText etRegisterBirthPlace;
    @Bind(R.id.et_register_birth_date)
    EditText etRegisterBirthDate;
    @Bind(R.id.et_register_email)
    EditText etRegisterEmail;
    @Bind(R.id.et_register_password)
    EditText etRegisterPassword;
    @Bind(R.id.et_register_address)
    EditText etRegisterAddress;
    @Bind(R.id.et_register_phone)
    EditText etRegisterPhone;
    @Bind(R.id.et_register_school)
    EditText etRegisterSchool;
    @Bind(R.id.et_register_graduate)
    EditText etRegisterGraduate;
    @Bind(R.id.spv_register_type)
    Spinner spvRegisterType;
    @Bind(R.id.et_register_religion)
    EditText etRegisterReligion;
    @Bind(R.id.et_register_dad_name)
    EditText etRegisterDadName;
    @Bind(R.id.et_register_dad_job)
    EditText etRegisterDadJob;
    @Bind(R.id.et_register_dad_school)
    EditText etRegisterDadSchool;
    @Bind(R.id.et_register_mom_name)
    EditText etRegisterMomName;
    @Bind(R.id.et_register_mom_job)
    EditText etRegisterMomJob;
    @Bind(R.id.et_register_mom_school)
    EditText etRegisterMomSchool;
    @Bind(R.id.et_register_parent_address)
    EditText etRegisterParentAddress;
    @Bind(R.id.et_register_child)
    EditText etRegisterChild;
    @Bind(R.id.et_register_brosis)
    EditText etRegisterBroSis;
    @Bind(R.id.btn_register_do_register)
    Button btnDoRegister;

    DetailPendaftar detailPendaftar;
    Calendar calendar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_pendaftar);
        setActionBarTitle(getMyBundle() == null ?
                getString(R.string.title_register) : getString(R.string.menu_change_profile));
        displayHome();

        loadData();
    }

    private void loadData(){
        String[] jenisPendaftaran = { "MI", "MTS", "MA"};

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, jenisPendaftaran);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spvRegisterType.setAdapter(spinnerArrayAdapter);

        calendar = Calendar.getInstance();
        detailPendaftar = new DetailPendaftar();
        if(getMyBundle() != null){
            showProgressDialog(getString(R.string.message_loading_data), false);
            RestApi restApi = RetrofitBuilder.create(RestApi.class);
            Call<ResultData<List<DetailPendaftar>>> request
                    = restApi.getListDetailPendaftar(
                    null, null, null, null, getMyBundle().getString("username"), null
            );
            request.enqueue(new ApiCallback<ResultData<List<DetailPendaftar>>>() {
                @Override
                public void onSuccess(ResultData<List<DetailPendaftar>> data) {
                    dismissProgressDialog();
                    if(data.getData().size() > 0){
                        detailPendaftar = data.getData().get(0);
                        implementView();
                    }
                }

                @Override
                public void onFailed(Throwable t) {
                    dismissProgressDialog();
                }
            });
        }
    }

    private void implementView(){
        etRegisterEmail.setText(detailPendaftar.getEmail());
        etRegisterEmail.setFocusable(false);
        etRegisterEmail.setClickable(false);
        int i = 0;
        do {
            spvRegisterType.setSelection(i);
            i++;
        }
        while(!spvRegisterType.getSelectedItem().equals(detailPendaftar.getJenisPendaftaran())
                && i < 3);
        etRegisterFullname.setText(detailPendaftar.getNamaCalon());
        if(detailPendaftar.getJenis_kelamin().equals("Laki-laki")){
            rbRegisterMan.setChecked(true);
        }else{
            rbRegisterWoman.setChecked(true);
        }
        String[] ttl = detailPendaftar.getTtl().split(" ");
        try{
            etRegisterBirthPlace.setText(ttl[0].replace(",", ""));
            etRegisterBirthDate.setText(ttl[1]+" "+ttl[2]+" "+ttl[3]);
        }catch (Exception e){

        }
        etRegisterReligion.setText(detailPendaftar.getAgama());
        etRegisterSchool.setText(detailPendaftar.getSekolahAsal());
        etRegisterGraduate.setText(detailPendaftar.getTahunIjazah());
        etRegisterChild.setText(detailPendaftar.getAnak());
        etRegisterBroSis.setText(detailPendaftar.getJumlahSaudara());
        etRegisterDadName.setText(detailPendaftar.getNamaAyah());
        etRegisterDadJob.setText(detailPendaftar.getPekerjaanAyah());
        etRegisterDadSchool.setText(detailPendaftar.getPendidikanAyah());
        etRegisterMomName.setText(detailPendaftar.getNamaIbu());
        etRegisterMomJob.setText(detailPendaftar.getPekerjaanIbu());
        etRegisterMomSchool.setText(detailPendaftar.getPendidikanIbu());
        etRegisterParentAddress.setText(detailPendaftar.getAlamatOrtu());
        etRegisterPhone.setText(detailPendaftar.getNoHp());
        etRegisterAddress.setText(detailPendaftar.getAlamat());
        btnDoRegister.setText(getString(R.string.do_update));
    }

    private void insertData(){
        detailPendaftar.setEmail(etRegisterEmail.getText().toString());
        detailPendaftar.setJenisPendaftaran((String) spvRegisterType.getSelectedItem());
        detailPendaftar.setNamaCalon(etRegisterFullname.getText().toString());
        detailPendaftar.setJenis_kelamin(rbRegisterMan.isChecked() ? "Laki-laki" : "Perempuan");
        detailPendaftar.setTtl(etRegisterBirthPlace.getText().toString() + ", "
                + etRegisterBirthDate.getText().toString());
        detailPendaftar.setAgama(etRegisterReligion.getText().toString());
        detailPendaftar.setSekolahAsal(etRegisterSchool.getText().toString());
        detailPendaftar.setTahunIjazah(etRegisterGraduate.getText().toString());
        detailPendaftar.setAlamat(etRegisterAddress.getText().toString());
        detailPendaftar.setAnak(etRegisterChild.getText().toString());
        detailPendaftar.setJumlahSaudara(etRegisterBroSis.getText().toString());
        detailPendaftar.setNamaAyah(etRegisterDadName.getText().toString());
        detailPendaftar.setNamaIbu(etRegisterMomName.getText().toString());
        detailPendaftar.setPekerjaanIbu(etRegisterMomJob.getText().toString());
        detailPendaftar.setPekerjaanAyah(etRegisterDadJob.getText().toString());
        detailPendaftar.setAlamatOrtu(etRegisterParentAddress.getText().toString());
        detailPendaftar.setPendidikanAyah(etRegisterDadSchool.getText().toString());
        detailPendaftar.setPendidikanIbu(etRegisterMomSchool.getText().toString());
        detailPendaftar.setNoHp(etRegisterPhone.getText().toString());
    }

    @OnClick(R.id.et_register_birth_date)
    public void onEtBirthDateClick(){
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar settedDate = Calendar.getInstance();
                        settedDate.set(year, monthOfYear, dayOfMonth);
                        etRegisterBirthDate.setText(
                                new SimpleDateFormat("dd MMM yyyy").format(settedDate.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }


    @OnClick(R.id.btn_register_do_register)
    public void onBtnDoRegisterClick(){
        insertData();
        if(getMyBundle() != null){
            showProgressDialog(getString(R.string.message_updating), false);
        }else{
            showProgressDialog(getString(R.string.message_registering), false);
        }
        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<Result> request = null;
        if(getMyBundle() == null) {
            request = restApi.postInsertPendaftar(
                    detailPendaftar.getEmail(), detailPendaftar.getJenisPendaftaran(),
                    detailPendaftar.getNamaCalon(), detailPendaftar.getJenis_kelamin(),
                    detailPendaftar.getTtl(), detailPendaftar.getAgama(),
                    detailPendaftar.getSekolahAsal(), detailPendaftar.getTahunIjazah(),
                    detailPendaftar.getAlamat(), detailPendaftar.getAnak(),
                    detailPendaftar.getJumlahSaudara(), detailPendaftar.getNamaAyah(),
                    detailPendaftar.getNamaIbu(), detailPendaftar.getPekerjaanAyah(),
                    detailPendaftar.getPekerjaanIbu(), detailPendaftar.getAlamatOrtu(),
                    detailPendaftar.getPendidikanAyah(), detailPendaftar.getPendidikanIbu(),
                    detailPendaftar.getNoHp(), etRegisterPassword.getText().toString()
            );
        }else{
            request = restApi.postUpdatePendaftar(detailPendaftar.getIdCalon(),
                    detailPendaftar.getEmail(), detailPendaftar.getJenisPendaftaran(),
                    detailPendaftar.getNamaCalon(), detailPendaftar.getJenis_kelamin(),
                    detailPendaftar.getTtl(), detailPendaftar.getAgama(),
                    detailPendaftar.getSekolahAsal(), detailPendaftar.getTahunIjazah(),
                    detailPendaftar.getAlamat(), detailPendaftar.getAnak(),
                    detailPendaftar.getJumlahSaudara(), detailPendaftar.getNamaAyah(),
                    detailPendaftar.getNamaIbu(), detailPendaftar.getPekerjaanAyah(),
                    detailPendaftar.getPekerjaanIbu(), detailPendaftar.getAlamatOrtu(),
                    detailPendaftar.getPendidikanAyah(), detailPendaftar.getPendidikanIbu(),
                    detailPendaftar.getNoHp(), etRegisterPassword.getText().toString()
            );
        }
        request.enqueue(new ApiCallback<Result>() {
            @Override
            public void onSuccess(Result data) {
                dismissProgressDialog();
                if(data.getMessage().equals("sukses")){
                    onBackPressed();
                }
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }
}
