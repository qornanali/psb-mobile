package com.almukhlisin.mobilepsb.ui.nilai;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.Nilai;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.Logger;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * Created by qornanali on 7/29/17.
 */

public class AddNilaiActivity extends BaseActivity {

    @Bind(R.id.et_nilai_baca)
    EditText etNilaiBaca;
    @Bind(R.id.et_nilai_tulis)
    EditText etNilaiTulis;
    @Bind(R.id.et_id_calon)
    EditText etIdCalon;

    Nilai nilai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_nilai);
        setActionBarTitle(getString(R.string.title_nilai));
        nilai = new Nilai();
        displayHome();
    }

    @OnClick(R.id.btn_do_add_nilai)
    public void onDoAddClick(){
        insertData();
        showProgressDialog(getString(R.string.message_adding_jadwal), false);
        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<Result> request = restApi.postInsertNilai(
                nilai.getIdCalon(), nilai.getNilaiBaca(), nilai.getNilaiTulis()
        );
        request.enqueue(new ApiCallback<Result>() {
            @Override
            public void onSuccess(Result data) {
                dismissProgressDialog();
                showSnackBar(data.getMessage());
                if(data.getMessage().equals("sukses")){
                    onBackPressed();
                }
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }

    private void insertData(){
        nilai.setIdCalon(etIdCalon.getText().toString());
        nilai.setNilaiBaca(etNilaiBaca.getText().toString());
        nilai.setNilaiTulis(etNilaiTulis.getText().toString());
    }
}
