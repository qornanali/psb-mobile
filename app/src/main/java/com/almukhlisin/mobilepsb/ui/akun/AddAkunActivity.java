package com.almukhlisin.mobilepsb.ui.akun;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.model.data.Akun;
import com.almukhlisin.mobilepsb.model.data.Nilai;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * Created by qornanali on 7/29/17.
 */

public class AddAkunActivity extends BaseActivity {

    @Bind(R.id.et_nama_lengkap)
    EditText etNamaLengkap;
    @Bind(R.id.et_username)
    EditText etUsername;
    @Bind(R.id.et_password)
    EditText etPassword;
    @Bind(R.id.et_no_hp)
    EditText etNoHp;
    @Bind(R.id.et_email)
    EditText etEmail;
    @Bind(R.id.spv_level)
    Spinner spvLevel;

    Akun akun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_akun);
        setActionBarTitle(getString(R.string.title_akun));
        akun = new Akun();
        displayHome();
        initView();
    }

    private void initView(){
        String[] jenisPendaftaran = {"Administrator", "Ketua Yayasan"};

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, jenisPendaftaran);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spvLevel.setAdapter(spinnerArrayAdapter);
    }

    @OnClick(R.id.btn_do_add_akun)
    public void onDoAddClick(){
        insertData();
        showProgressDialog(getString(R.string.message_adding_akun), false);
        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<Result> request = restApi.postInsertAkun(
          akun.getUsername(), etPassword.getText().toString(),
                akun.getEmail(), akun.getNamaLengkap(),
                akun.getNoHp(), akun.getLevel()
        );
        request.enqueue(new ApiCallback<Result>() {
            @Override
            public void onSuccess(Result data) {
                dismissProgressDialog();
                showSnackBar(data.getMessage());
                if(data.getMessage().equals("sukses")){
                    onBackPressed();
                }
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }

    private void insertData(){
        akun.setUsername(etUsername.getText().toString());
        akun.setEmail(etEmail.getText().toString());
        akun.setNamaLengkap(etNamaLengkap.getText().toString());
        akun.setLevel((String) spvLevel.getSelectedItem());
        akun.setNoHp(etNoHp.getText().toString());
    }
}
