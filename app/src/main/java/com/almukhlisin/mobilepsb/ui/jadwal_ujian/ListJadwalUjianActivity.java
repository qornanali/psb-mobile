package com.almukhlisin.mobilepsb.ui.jadwal_ujian;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;

import com.almukhlisin.mobilepsb.BuildConfig;
import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.DetailPendaftar;
import com.almukhlisin.mobilepsb.model.data.JadwalUjian;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.adapter.ListJadwalUjianAdapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.orhanobut.hawk.Hawk;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

public class ListJadwalUjianActivity extends BaseActivity {

    @Bind(R.id.rv_list_jadwal_ujian)
    RecyclerView rvListJadwalUjian;
    @Bind(R.id.btn_do_schedule)
    Button btnDoSchedule;

    List<JadwalUjian> jadwalUjien;
    ListJadwalUjianAdapter adapter;
    Auth auth;
    DetailPendaftar detailPendaftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_jadwal_ujian);
        setActionBarTitle(getString(R.string.menu_exam));
        displayHome();
        initView();
        loadData();
    }

    private void initView(){
        jadwalUjien = new ArrayList<>();
        adapter = new ListJadwalUjianAdapter(getMyContext(), jadwalUjien) {
            @Override
            public void onItemClick(int position) {
            }
        };
        rvListJadwalUjian.setLayoutManager(new LinearLayoutManager(getMyContext()));
        rvListJadwalUjian.setAdapter(adapter);
    }

    @OnClick(R.id.btn_do_schedule)
    public void onBtnDoScheduleClick(){
        if(auth.getLevel().equals("Pendaftar")){
            printSchedule();
        }else{
            goToActivity(AddJadwalActivity.class, null, false);
        }
    }

    private void printSchedule() {
        try {
            String filePath = Environment.getExternalStorageDirectory()+"/"+getString(R.string.app_id) + "/jadwal_ujian-" +
                    System.currentTimeMillis() + ".pdf";
            File pdfFolder = new File(Environment.getExternalStorageDirectory()+"/"+getString(R.string.app_id), "jadwal_ujian-" +
                    System.currentTimeMillis() + ".pdf");
            Logger.log(Log.ERROR, getMyContext().getFilesDir()+"/"+getString(R.string.app_id));
            if (!pdfFolder.exists()) {
                Logger.log(Log.ERROR, "doesnt exist");
                if(!pdfFolder.getParentFile().mkdir()){
                    Logger.log(Log.ERROR, "failed to mkdir");
                }
            }else{
                Logger.log(Log.ERROR, "exist");
            }

            //Create time stamp

            File myFile = new File(filePath);

            OutputStream output = new FileOutputStream(myFile);

            //Step 1
            Document document = new Document();

            //Step 2
                PdfWriter.getInstance(document, output);
            //Step 3
            document.open();

            //Step 4 Add content
            document.addCreationDate();
            document.add(new Paragraph(getString(R.string.al_mukhlisin)));
            document.add(new Paragraph(getString(R.string.address)));
            document.addTitle(getString(R.string.menu_exam));
            document.add(new Paragraph("\n\n"));
            if(detailPendaftar.getPasFoto().equals("")){
                document.add(Image.getInstance(new URL(BuildConfig.BASE_URL+detailPendaftar.getPasFoto())));
            }
            document.add(new Paragraph(getString(R.string.id_calon)+" : " + detailPendaftar.getIdCalon()));
            document.add(new Paragraph(getString(R.string.field_full_name)+" : " + detailPendaftar.getNamaCalon()));
            document.add(new Paragraph(getString(R.string.field_birth_place)+", " + getString(R.string.field_birth_date) +
                    " : " + detailPendaftar.getTtl()));
            document.add(new Paragraph("\n"));
            document.add(new LineSeparator());
            document.add(new Paragraph("\n\n"));
            for(JadwalUjian jadwalUjian : jadwalUjien){
                document.add(new Paragraph(
                        getString(R.string.field_tanggal_ujian) + " : " + jadwalUjian.getTanggalUjian()+"\n"+
                        getString(R.string.field_waktu_ujian) + " : " + jadwalUjian.getWaktuUjian()+"\n"+
                                getString(R.string.field_jenis_ujian) + " : " + jadwalUjian.getJenisUjian()+"\n"
                ));
                document.add(new Paragraph("\n"));
            }
            //Step 5: Close the document
        document.close();
            showSnackBar("tersimpan di : " + filePath);
        } catch (DocumentException e) {
            e.printStackTrace();
            showSnackBar(e.getMessage());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            showSnackBar(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            showSnackBar(e.getMessage());
        }
    }

    private void loadData(){
        auth = Hawk.get("auth");
        detailPendaftar = Hawk.get("detail_pendaftar");
        showProgressDialog(getString(R.string.message_loading_data), false);
        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<ResultData<List<JadwalUjian>>> request = null;
        if(auth.getLevel().equals("Pendaftar")){
            DetailPendaftar detailPendaftar = Hawk.get("detail_pendaftar");
            request = restApi.getListJadwalUjian("true", detailPendaftar.getJenisPendaftaran()
            );
            btnDoSchedule.setText(getString(R.string.do_print_schedule));
        }else{
            request = restApi.getListJadwalUjian(
                    null, null
            );
            btnDoSchedule.setText(getString(R.string.do_add_schedule));
        }
        request.enqueue(new ApiCallback<ResultData<List<JadwalUjian>>>() {
            @Override
            public void onSuccess(ResultData<List<JadwalUjian>> data) {
                dismissProgressDialog();
                for (int i = 0; i < data.getData().size(); i++) {
                    jadwalUjien.add(data.getData().get(i));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }
}
