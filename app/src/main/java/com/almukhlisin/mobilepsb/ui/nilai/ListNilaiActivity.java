package com.almukhlisin.mobilepsb.ui.nilai;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.DetailPendaftar;
import com.almukhlisin.mobilepsb.model.data.Nilai;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.adapter.ListNilaiAdapter;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

public class ListNilaiActivity extends BaseActivity {

    @Bind(R.id.rv_list_nilai)
    RecyclerView rvListNilai;
    @Bind(R.id.btn_do_add_nilai)
    Button btnDoAddNilai;

    List<Nilai> nilaiList;
    ListNilaiAdapter adapter;
    Auth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_nilai);
        setActionBarTitle(getString(R.string.menu_score));
        displayHome();
        initView();
        loadData();
    }

    private void initView(){
        nilaiList = new ArrayList<>();
        adapter = new ListNilaiAdapter(getMyContext(), nilaiList) {
            @Override
            public void onItemClick(int position) {
            }
        };
        rvListNilai.setLayoutManager(new LinearLayoutManager(getMyContext()));
        rvListNilai.setAdapter(adapter);
    }

    @OnClick(R.id.btn_do_add_nilai)
    public void onBtnDoAddNilaiClick(){
        goToActivity(AddNilaiActivity.class, null, false);
    }

    private void loadData(){
        auth = Hawk.get("auth");
        btnDoAddNilai.setVisibility(auth.getLevel().equals("Pendaftar") ?
                    View.GONE : View.VISIBLE);
        showProgressDialog(getString(R.string.message_loading_data), false);
        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<ResultData<List<Nilai>>> request = null;
        if(!auth.getLevel().equals("Pendaftar")){
            request = restApi.getListNilai(
                    null, null, null, null, null
            );
        }else{
            DetailPendaftar detailPendaftar = Hawk.get("detail_pendaftar");
            request = restApi.getListNilai(
                    null, null, null, detailPendaftar.getIdCalon(), null
            );
        }
        request.enqueue(new ApiCallback<ResultData<List<Nilai>>>() {
            @Override
            public void onSuccess(ResultData<List<Nilai>> data) {
                dismissProgressDialog();
                for (int i = 0; i < data.getData().size(); i++) {
                    nilaiList.add(data.getData().get(i));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }
}
