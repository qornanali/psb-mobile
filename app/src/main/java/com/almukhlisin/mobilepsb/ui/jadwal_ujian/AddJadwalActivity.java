package com.almukhlisin.mobilepsb.ui.jadwal_ujian;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.JadwalUjian;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.Logger;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * Created by qornanali on 7/29/17.
 */

public class AddJadwalActivity extends BaseActivity {

    @Bind(R.id.spv_jenis_ujian)
    Spinner spvJenisUjian;
    @Bind(R.id.et_tanggal_ujian)
    EditText etTanggalUjian;
    @Bind(R.id.et_waktu_ujian)
    EditText etWaktuUjian;
    @Bind(R.id.et_ruangan)
    EditText etRuangan;
    @Bind(R.id.spv_register_type)
    Spinner spvJenisPendaftaran;

    JadwalUjian jadwalUjian;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_jadwal_ujian);
        setActionBarTitle(getString(R.string.title_jadwal));
        displayHome();
        loadData();
    }

    @OnClick(R.id.btn_do_add_schedule)
    public void onDoAddClick(){
        insertData();
        showProgressDialog(getString(R.string.message_adding_jadwal), false);
        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<Result> request = restApi.postInsertJadwalUjian(
                jadwalUjian.getJenisUjian(), jadwalUjian.getTanggalUjian(),
                jadwalUjian.getWaktuUjian(), jadwalUjian.getJenisPendaftaran(),
                jadwalUjian.getRuangan()
        );
        request.enqueue(new ApiCallback<Result>() {
            @Override
            public void onSuccess(Result data) {
                dismissProgressDialog();
                showSnackBar(data.getMessage());
                if(data.getMessage().equals("sukses")){
                    onBackPressed();
                }
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }

    private void loadData(){
        jadwalUjian = new JadwalUjian();

        String[] jenisPendaftaran = { "MI", "MTS", "MA"};
        ArrayAdapter<String> spinnerjenisPendaftaranAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, jenisPendaftaran);
        spinnerjenisPendaftaranAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spvJenisPendaftaran.setAdapter(spinnerjenisPendaftaranAdapter);

        String[] jenisUjian = { "Ujian baca", "Ujian tulis"};
        ArrayAdapter<String> spinnerjenisUjianAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, jenisUjian);
        spinnerjenisUjianAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spvJenisUjian.setAdapter(spinnerjenisUjianAdapter);


        calendar = Calendar.getInstance();
    }


    @OnClick(R.id.et_tanggal_ujian)
    public void onEtTanggalUjianClick(){
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(year, monthOfYear, dayOfMonth);
                        etTanggalUjian.setText(
                                new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }


    @OnClick(R.id.et_waktu_ujian)
    public void onEtWaktuUjianClick(){
        TimePickerDialog tpd = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
                calendar.set(calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DATE),
                        hourOfDay,
                        minute);
                etWaktuUjian.setText(
                        new SimpleDateFormat("hh:mm").format(calendar.getTime()));
            }
        }, calendar.get(Calendar.HOUR),
                calendar.get(Calendar.MINUTE), true);
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    private void insertData(){
        jadwalUjian.setJenisPendaftaran((String) spvJenisPendaftaran.getSelectedItem());
        jadwalUjian.setJenisUjian((String) spvJenisUjian.getSelectedItem());
        jadwalUjian.setRuangan(etRuangan.getText().toString());
        jadwalUjian.setTanggalUjian(etTanggalUjian.getText().toString());
        jadwalUjian.setWaktuUjian(etWaktuUjian.getText().toString());
    }
}
