package com.almukhlisin.mobilepsb.ui.pengumuman;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.DetailPendaftar;
import com.almukhlisin.mobilepsb.model.data.Nilai;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.adapter.ListLulusAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * Created by qornanali on 7/29/17.
 */

public class UpdatePengumumanActivity extends BaseActivity {

    @Bind(R.id.rv_list_lulus)
    RecyclerView rvListLulus;

    List<Nilai> lulusList;
    ListLulusAdapter adapter;
    RestApi restApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_pengumuman);
        restApi = RetrofitBuilder.create(RestApi.class);
        setActionBarTitle(getString(R.string.menu_announcement));
        displayHome();
        initView();
        loadData();
    }

    private void loadData(){
        showProgressDialog(getString(R.string.message_loading_data), false);
        Call<ResultData<List<Nilai>>> request = restApi.getListNilai(
                null, null, null, null, "true"
        );
        request.enqueue(new ApiCallback<ResultData<List<Nilai>>>() {
            @Override
            public void onSuccess(ResultData<List<Nilai>> data) {
                dismissProgressDialog();
                for (int i = 0; i < data.getData().size(); i++) {
                    lulusList.add(data.getData().get(i));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }

    private void initView(){
        lulusList = new ArrayList<>();
        adapter = new ListLulusAdapter(getMyContext(), lulusList) {
            @Override
            public void onItemClick(int position) {
            }

            @Override
            public void onSmsClick(int position) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    int hasSMSPermission = checkSelfPermission(Manifest.permission.SEND_SMS);
                    if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                        if (!shouldShowRequestPermissionRationale(Manifest.permission.SEND_SMS)) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{Manifest.permission.SEND_SMS},
                                        0);
                            }
                            return;
                        }
                        requestPermissions(new String[]{Manifest.permission.SEND_SMS},
                                0);
                        return;
                    }
                }
                sendMySms(position);
            }
        };
        rvListLulus.setLayoutManager(new LinearLayoutManager(getMyContext()));
        rvListLulus.setAdapter(adapter);
    }

    private void sendMySms(int position){
        restApi = RetrofitBuilder.create(RestApi.class);
        Call<ResultData<List<DetailPendaftar>>> resultDataCall
                = restApi.getListDetailPendaftar(
                null, null, null, null, null, lulusList.get(position).getIdCalon()
        );
        resultDataCall.enqueue(new ApiCallback<ResultData<List<DetailPendaftar>>>() {
            @Override
            public void onSuccess(ResultData<List<DetailPendaftar>> data) {
                if(data.getData().size() > 0){
                    String phone = data.getData().get(0).getNoHp();
                    if (phone.isEmpty()) {
                        showToast(getString(R.string.message_phone_not_valid));
                    } else {
                        String message = getString(R.string.message_congratulations)
                                + " " + getString(R.string.message_accepted) + ".Untuk itu silahkan mengirimkan foto bukti daftar ulang!";
                        SmsManager sms = SmsManager.getDefault();
                        List<String> messages = sms.divideMessage(message);
                        for (String msg : messages) {
                            PendingIntent sentIntent = PendingIntent.getBroadcast(getMyContext(), 0, new Intent("SMS_SENT"), 0);
                            PendingIntent deliveredIntent = PendingIntent.getBroadcast(getMyContext(), 0, new Intent("SMS_DELIVERED"), 0);
                            sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);
                        }
                        showSnackBar("Sms pengumuman terkirim");
                    }
                }
            }

            @Override
            public void onFailed(Throwable t) {
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }

    @OnClick(R.id.btn_do_update_pengumuman)
    public void onBtnUpdatePengumumanClick(){
        showProgressDialog(getString(R.string.message_updating));
        Call<Result> resultCall = restApi.postUpdatePengumuman();
        resultCall.enqueue(new ApiCallback<Result>() {
            @Override
            public void onSuccess(Result data) {
                dismissProgressDialog();
                showSnackBar(data.getMessage());
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }

}
