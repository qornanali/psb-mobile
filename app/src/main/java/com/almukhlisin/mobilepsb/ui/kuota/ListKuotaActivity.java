package com.almukhlisin.mobilepsb.ui.kuota;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.model.data.Akun;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.Kuota;
import com.almukhlisin.mobilepsb.model.data.ResultData;
import com.almukhlisin.mobilepsb.ui.base.BaseActivity;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.util.adapter.ListAkunAdapter;
import com.almukhlisin.mobilepsb.util.adapter.ListKuotaAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Call;

public class ListKuotaActivity extends BaseActivity {

    @Bind(R.id.rv_list_kuota)
    RecyclerView rvListKuota;

    List<Kuota> kuotaList;
    ListKuotaAdapter adapter;
    Auth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_kuota);
        setActionBarTitle(getString(R.string.menu_manage_kuota));
        displayHome();
        initView();
        loadData();
    }

    private void initView(){
        kuotaList = new ArrayList<>();
        adapter = new ListKuotaAdapter(getMyContext(), kuotaList) {
            @Override
            public void onItemClick(int position) {
            }
        };
        rvListKuota.setLayoutManager(new LinearLayoutManager(getMyContext()));
        rvListKuota.setAdapter(adapter);
    }

    @OnClick(R.id.btn_do_add_kuota)
    public void onBtnDoAddKuotaClick(){
        goToActivity(AddKuotaActivity.class, null, false);
    }

    private void loadData(){
        showProgressDialog(getString(R.string.message_loading_data), false);
        RestApi restApi = RetrofitBuilder.create(RestApi.class);
        Call<ResultData<List<Kuota>>> request =
                restApi.getListKuota(
                    null, null, null, null
            );
        request.enqueue(new ApiCallback<ResultData<List<Kuota>>>() {
            @Override
            public void onSuccess(ResultData<List<Kuota>> data) {
                dismissProgressDialog();
                for (int i = 0; i < data.getData().size(); i++) {
                    kuotaList.add(data.getData().get(i));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                dismissProgressDialog();
                Logger.log(Log.ERROR, t.getMessage());
            }
        });
    }
}
