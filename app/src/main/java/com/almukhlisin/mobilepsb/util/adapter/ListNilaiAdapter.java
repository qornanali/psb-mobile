package com.almukhlisin.mobilepsb.util.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.almukhlisin.mobilepsb.BuildConfig;
import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.Nilai;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;

public abstract class ListNilaiAdapter extends RecyclerView.Adapter<ListNilaiAdapter.nilaiItemHolder> {

    private List<Nilai> nilaiList;
    private Context context;
    private View itemView;


    class nilaiItemHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_nilai_baca)
        TextView tvNilaiBaca;
        @Bind(R.id.tv_nilai_id_calon)
        TextView tvNilaiIdCalon;
        @Bind(R.id.tv_nilai_jenis_pendaftaran)
        TextView tvNilaiJenisPendaftaran;
        @Bind(R.id.tv_nilai_nama_calon)
        TextView tvNamaCalon;
        @Bind(R.id.tv_nilai_status_kelulusan)
        TextView tvNilaiStatusKelulusan;
        @Bind(R.id.tv_nilai_tulis)
        TextView tvNilaiTulis;
        @Bind(R.id.iv_nilai_pas_foto)
        ImageView ivNilaiPasFoto;
        @Bind(R.id.btn_do_remove)
        Button btnDoRemove;

        Nilai item;

        public nilaiItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ListNilaiAdapter(Context context, List<Nilai> nilaiList) {
        this.nilaiList = nilaiList;
        this.context = context;
    }

    @Override
    public nilaiItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_nilai, parent, false);

        return new nilaiItemHolder(itemView);
    }

    public abstract void onItemClick(int position);

    @Override
    public void onBindViewHolder(final nilaiItemHolder holder, final int position) {
        holder.item = nilaiList.get(position);
       holder.tvNamaCalon.setText(
               holder.item.getNamaCalon()
       );
       holder.tvNilaiBaca.setText(
               context.getString(R.string.field_nilai_baca)+" : " + holder.item.getNilaiBaca()
       );
       holder.tvNilaiTulis.setText(
               context.getString(R.string.field_nilai_tulis)+" : " + holder.item.getNilaiTulis()
       );
       holder.tvNilaiIdCalon.setText(
               holder.item.getIdCalon()
       );
       holder.tvNilaiJenisPendaftaran.setText(
               context.getString(R.string.field_register_type)+" : " + holder.item.getJenisPendaftaran()
       );
       holder.tvNilaiStatusKelulusan.setText(
               holder.item.getStatusKelulusan()
       );
        holder.tvNilaiStatusKelulusan.setTextColor(
                context.getResources().getColor(
                        holder.item.getStatusKelulusan().equals("Diterima")
                                ? R.color.green : R.color.red));
        Auth auth = Hawk.get("auth");
        if(auth != null){
            if(auth.getLevel().equals("Administrator")){
                holder.btnDoRemove.setVisibility(View.VISIBLE);
                holder.btnDoRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        RestApi restApi = RetrofitBuilder.create(RestApi.class);
                        Call<Result> resultCall = restApi.postDeleteNilai(
                                holder.item.getIdCalon()
                        );
                        resultCall.enqueue(new ApiCallback<Result>() {
                            @Override
                            public void onSuccess(Result data) {
                                Toast.makeText(context, data.getMessage(), Toast.LENGTH_LONG).show();
                                if(data.getMessage().equals("sukses")){
                                    nilaiList.remove(position);
                                    notifyItemRemoved(position);
                                }
                            }

                            @Override
                            public void onFailed(Throwable t) {

                            }
                        });
                    }
                });
            }
        }

        if(!holder.item.getPasFoto().equals("")){
            Picasso.with(context).load(BuildConfig.BASE_URL+holder.item.getPasFoto())
                    .resize(80, 80).error(R.color.gray)
                    .into(holder.ivNilaiPasFoto);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return nilaiList.size();
    }
}