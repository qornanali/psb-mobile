package com.almukhlisin.mobilepsb.util.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.almukhlisin.mobilepsb.BuildConfig;
import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.Nilai;
import com.almukhlisin.mobilepsb.util.Logger;
import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public abstract class ListLulusAdapter extends RecyclerView.Adapter<ListLulusAdapter.nilaiItemHolder> {

    private List<Nilai> nilaiList;
    private Context context;
    private View itemView;


    class nilaiItemHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_nilai_id_calon)
        TextView tvNilaiIdCalon;
        @Bind(R.id.tv_nilai_jenis_pendaftaran)
        TextView tvNilaiJenisPendaftaran;
        @Bind(R.id.tv_nilai_nama_calon)
        TextView tvNamaCalon;
        @Bind(R.id.iv_nilai_pas_foto)
        ImageView ivNilaiPasFoto;
        @Bind(R.id.btn_do_send)
        Button btnDoSend;

        Nilai item;

        public nilaiItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ListLulusAdapter(Context context, List<Nilai> nilaiList) {
        this.nilaiList = nilaiList;
        this.context = context;
    }

    @Override
    public nilaiItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lulus, parent, false);

        return new nilaiItemHolder(itemView);
    }

    public abstract void onItemClick(int position);
    public abstract void onSmsClick(int position);

    @Override
    public void onBindViewHolder(final nilaiItemHolder holder, final int position) {
        holder.item = nilaiList.get(position);
       holder.tvNamaCalon.setText(
               holder.item.getNamaCalon()
       );
       holder.tvNilaiIdCalon.setText(
               holder.item.getIdCalon()
       );
       holder.tvNilaiJenisPendaftaran.setText(
               context.getString(R.string.field_register_type)+" : " + holder.item.getJenisPendaftaran()
       );
        Auth auth = Hawk.get("auth");
        if(auth != null){
            if(auth.getLevel().equals("Administrator")){
                holder.btnDoSend.setVisibility(View.VISIBLE);
                holder.btnDoSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onSmsClick(position);
                    }
                });
            }
        }

        if(!holder.item.getPasFoto().equals("")){
            Picasso.with(context).load(BuildConfig.BASE_URL+holder.item.getPasFoto())
                    .resize(80, 80).error(R.color.gray)
                    .into(holder.ivNilaiPasFoto);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return nilaiList.size();
    }
}