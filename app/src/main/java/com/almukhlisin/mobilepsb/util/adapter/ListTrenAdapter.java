package com.almukhlisin.mobilepsb.util.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.model.data.Tren;
import com.almukhlisin.mobilepsb.model.other.Laporan;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public abstract class ListTrenAdapter extends RecyclerView.Adapter<ListTrenAdapter.trenItemHolder> {

    private List<Tren> trenList;
    private Context context;
    private View itemView;


    class trenItemHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tren_content)
        TextView trenContent;
        @Bind(R.id.tren_title)
        TextView trenTitle;

        Tren item;

        public trenItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ListTrenAdapter(Context context, List<Tren> trenList) {
        this.trenList = trenList;
        this.context = context;
    }

    @Override
    public trenItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tren, parent, false);

        return new trenItemHolder(itemView);
    }

    public abstract void onItemClick(int position);

    @Override
    public void onBindViewHolder(final trenItemHolder holder, final int position) {
        holder.item = trenList.get(position);

        holder.trenContent.setText(
                holder.item.getTrenContent()
        );

        holder.trenTitle.setText(
                holder.item.getTrenTitle()
        );

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return trenList.size();
    }
}