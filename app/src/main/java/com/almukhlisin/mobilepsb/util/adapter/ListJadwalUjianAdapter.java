package com.almukhlisin.mobilepsb.util.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.util.Logger;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.JadwalUjian;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.orhanobut.hawk.Hawk;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;

public abstract class ListJadwalUjianAdapter extends RecyclerView.Adapter<ListJadwalUjianAdapter.JadwalUjianItemHolder> {

    private List<JadwalUjian> jadwalUjien;
    private Context context;
    private View itemView;


    class JadwalUjianItemHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_jadwal_ujian_jenis_ujian)
        TextView tvJadwalUjianJenisUjian;
        @Bind(R.id.tv_jadwal_ujian_tanggal_ujian)
        TextView tvJadwalUjianTanggalUjian;
        @Bind(R.id.tv_jadwal_ujian_waktu_ujian)
        TextView tvJadwalUjianWaktuUjian;
        @Bind(R.id.tv_jadwal_ujian_ruangan)
        TextView tvJadwalUjianRuangan;
        @Bind(R.id.btn_do_remove)
        Button btnDoRemove;

        JadwalUjian item;

        public JadwalUjianItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ListJadwalUjianAdapter(Context context, List<JadwalUjian> jadwalUjien) {
        this.jadwalUjien = jadwalUjien;
        this.context = context;
    }

    @Override
    public JadwalUjianItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_jadwal_ujian, parent, false);

        return new JadwalUjianItemHolder(itemView);
    }

    public abstract void onItemClick(int position);

    @Override
    public void onBindViewHolder(final JadwalUjianItemHolder holder, final int position) {
        holder.item = jadwalUjien.get(position);

       holder.tvJadwalUjianJenisUjian.setText(
               context.getString(R.string.field_jenis_ujian)+" : " + holder.item.getJenisUjian()
       );
       holder.tvJadwalUjianTanggalUjian.setText(
               context.getString(R.string.field_tanggal_ujian)+" : " + holder.item.getTanggalUjian()
       );
       holder.tvJadwalUjianWaktuUjian.setText(
               context.getString(R.string.field_waktu_ujian)+" : " + holder.item.getWaktuUjian()
       );
       holder.tvJadwalUjianRuangan.setText(
               context.getString(R.string.field_ruangan)+" : " + holder.item.getRuangan()
       );
        Auth auth = Hawk.get("auth");
        if(auth != null){
            if(auth.getLevel().equals("Administrator")){
                holder.btnDoRemove.setVisibility(View.VISIBLE);
                holder.btnDoRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        RestApi restApi = RetrofitBuilder.create(RestApi.class);
                        Call<Result> resultCall = restApi.postDeleteJadwalUjian(
                                holder.item.getIdJadwal()
                        );
                        resultCall.enqueue(new ApiCallback<Result>() {
                            @Override
                            public void onSuccess(Result data) {
                                Toast.makeText(context, data.getMessage(), Toast.LENGTH_LONG).show();
                                if(data.getMessage().equals("sukses")){
                                    jadwalUjien.remove(position);
                                    notifyItemRemoved(position);
                                }
                            }

                            @Override
                            public void onFailed(Throwable t) {

                            }
                        });
                    }
                });
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return jadwalUjien.size();
    }
}