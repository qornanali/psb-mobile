package com.almukhlisin.mobilepsb.util;

import android.support.annotation.NonNull;
import android.util.JsonReader;

import com.almukhlisin.mobilepsb.BuildConfig;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class RetrofitBuilder {


    @NonNull
    private static Retrofit getRetrofit() {

        OkHttpClient okHttpKlien = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addNetworkInterceptor(new ServiceInterceptor())
                .build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpKlien)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static RestApi create(Class c){
        return (RestApi) getRetrofit().create(c);
    }

}