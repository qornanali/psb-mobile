package com.almukhlisin.mobilepsb.util.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.model.data.Akun;
import com.almukhlisin.mobilepsb.model.data.Kuota;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;

public abstract class ListKuotaAdapter extends RecyclerView.Adapter<ListKuotaAdapter.kuotaItemHolder> {

    private List<Kuota> kuotaList;
    private Context context;
    private View itemView;


    class kuotaItemHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_kuota_jenis_pendaftaran)
        TextView tvKuotaJenisPendaftaran;
        @Bind(R.id.tv_kuota_jumlah)
        TextView tvKuotaJumlah;
        @Bind(R.id.tv_kuota_tahun)
        TextView tvKuotaTahun;
        @Bind(R.id.btn_do_remove)
        Button btnDoRemove;

        Kuota item;

        public kuotaItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ListKuotaAdapter(Context context, List<Kuota> kuotaList) {
        this.kuotaList = kuotaList;
        this.context = context;
    }

    @Override
    public kuotaItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_kuota, parent, false);

        return new kuotaItemHolder(itemView);
    }

    public abstract void onItemClick(int position);

    @Override
    public void onBindViewHolder(final kuotaItemHolder holder, final int position) {
        holder.item = kuotaList.get(position);

        holder.tvKuotaJenisPendaftaran.setText(
                context.getString(R.string.field_register_type) + " : " +
                        holder.item.getJenisPendaftaran()
        );
        holder.tvKuotaJumlah.setText(
                context.getString(R.string.field_jumlah) + " : " +
                        holder.item.getJumlah()
        );
        holder.tvKuotaTahun.setText(
                context.getString(R.string.field_tahun) + " : " + holder.item.getTahun()
        );

                holder.btnDoRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        RestApi restApi = RetrofitBuilder.create(RestApi.class);
                        Call<Result> resultCall = restApi.postDeleteKuota(
                                holder.item.getIdKuota()
                        );
                        resultCall.enqueue(new ApiCallback<Result>() {
                            @Override
                            public void onSuccess(Result data) {
                                Toast.makeText(context, data.getMessage(), Toast.LENGTH_LONG).show();
                                if(data.getMessage().equals("sukses")){
                                    kuotaList.remove(position);
                                    notifyItemRemoved(position);
                                }
                            }

                            @Override
                            public void onFailed(Throwable t) {

                            }
                        });
                    }
                });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return kuotaList.size();
    }
}