package com.almukhlisin.mobilepsb.util.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.almukhlisin.mobilepsb.BuildConfig;
import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.Nilai;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.model.other.Laporan;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;

public abstract class ListLaporanAdapter extends RecyclerView.Adapter<ListLaporanAdapter.laporanItemHolder> {

    private List<Laporan> laporanList;
    private Context context;
    private View itemView;


    class laporanItemHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_laporan_jenis_pendaftaran)
        TextView tvLaporanJenisPendaftaran;
        @Bind(R.id.tv_laporan_nama)
        TextView tvLaporanNama;
        @Bind(R.id.tv_laporan_nilai_baca)
        TextView tvLaporanNilaiBaca;
        @Bind(R.id.tv_laporan_nilai_tulis)
        TextView tvLaporanNilaiTulis;
        @Bind(R.id.ll_laporan_row)
        LinearLayout llLaporanRow;

        Laporan item;

        public laporanItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ListLaporanAdapter(Context context, List<Laporan> laporanList) {
        this.laporanList = laporanList;
        this.context = context;
    }

    @Override
    public laporanItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_laporan, parent, false);

        return new laporanItemHolder(itemView);
    }

    public abstract void onItemClick(int position);

    @Override
    public void onBindViewHolder(final laporanItemHolder holder, final int position) {
        holder.item = laporanList.get(position);

        holder.tvLaporanJenisPendaftaran.setText(
               holder.item.getField2()
       );

        holder.tvLaporanNama.setText(
               holder.item.getField1()
       );

        holder.tvLaporanNilaiBaca.setText(
               holder.item.getField3()
       );

        holder.tvLaporanNilaiTulis.setText(
               holder.item.getField4()
       );

        if(position == 0){
            holder.llLaporanRow.setBackgroundColor(
                    context.getResources().getColor(R.color.green)
            );
            holder.tvLaporanJenisPendaftaran.setTextColor(
                    context.getResources().getColor(R.color.white)
            );
            holder.tvLaporanNilaiTulis.setTextColor(
                    context.getResources().getColor(R.color.white)
            );
            holder.tvLaporanNama.setTextColor(
                    context.getResources().getColor(R.color.white)
            );
            holder.tvLaporanNilaiBaca.setTextColor(
                    context.getResources().getColor(R.color.white)
            );
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return laporanList.size();
    }
}