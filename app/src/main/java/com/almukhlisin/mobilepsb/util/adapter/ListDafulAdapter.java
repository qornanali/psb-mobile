package com.almukhlisin.mobilepsb.util.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.almukhlisin.mobilepsb.BuildConfig;
import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.model.data.Auth;
import com.almukhlisin.mobilepsb.model.data.Daful;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.ui.base.ImageViewerActivity;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;
import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;

public abstract class ListDafulAdapter extends RecyclerView.Adapter<ListDafulAdapter.dafulItemHolder> {

    private List<Daful> dafulList;
    private Context context;
    private View itemView;


    class dafulItemHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_daful_id_calon)
        TextView tvDafulIdCalon;
        @Bind(R.id.tv_daful_jenis_pendaftaran)
        TextView tvDafulJenisPendaftaran;
        @Bind(R.id.tv_daful_status)
        TextView tvDafulStatus;
        @Bind(R.id.tv_daful_tanggal_pembayaran)
        TextView tvDafulTanggalPembayaran;
        @Bind(R.id.iv_daful_bukti)
        ImageView ivDafulBukti;
        @Bind(R.id.btn_do_confirmation)
        Button btnDoConfirmation;

        Daful item;

        public dafulItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ListDafulAdapter(Context context, List<Daful> dafulList) {
        this.dafulList = dafulList;
        this.context = context;
    }

    @Override
    public dafulItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_daful, parent, false);

        return new dafulItemHolder(itemView);
    }

    public abstract void onItemClick(int position);

    @Override
    public void onBindViewHolder(final dafulItemHolder holder, final int position) {
        holder.item = dafulList.get(position);

        holder.tvDafulIdCalon.setText(
                context.getString(R.string.id_calon) + " : " + holder.item.getIdCalon()
        );
        holder.tvDafulJenisPendaftaran.setText(
                context.getString(R.string.field_register_type) + " : " +
                        holder.item.getJenisPendaftaran()
        );
        holder.tvDafulTanggalPembayaran.setText(
                context.getString(R.string.field_paid_date) + " : " +
                        holder.item.getTanggalPembayaran()
        );
        holder.tvDafulStatus.setText(
                holder.item.getStatus()
        );

        holder.tvDafulStatus.setTextColor(
                context.getResources().getColor(
                        holder.item.getStatus().equals("Sudah Dikonfirmasi")
                                ? R.color.green : R.color.red));
        if(!holder.item.getStatus().equals("Sudah Dikonfirmasi")){
                holder.btnDoConfirmation.setVisibility(View.VISIBLE);
                holder.btnDoConfirmation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        RestApi restApi = RetrofitBuilder.create(RestApi.class);
                        Call<Result> resultCall = restApi.postVerifyDaftarUlang(
                                holder.item.getIdCalon()
                        );
                        resultCall.enqueue(new ApiCallback<Result>() {
                            @Override
                            public void onSuccess(Result data) {
                                Toast.makeText(context, data.getMessage(), Toast.LENGTH_LONG).show();
                                if(data.getMessage().equals("sukses")){
                                    dafulList.get(position).setStatus("Sudah Dikonfirmasi");
                                    notifyItemChanged(position);
                                }
                            }

                            @Override
                            public void onFailed(Throwable t) {

                            }
                        });
                    }
                });
            }

        if(!holder.item.getBukti().equals("")){
            Picasso.with(context).load(BuildConfig.BASE_URL+holder.item.getBukti())
                    .resize(80, 80).error(R.color.gray)
                    .into(holder.ivDafulBukti);
            holder.ivDafulBukti.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("url", holder.item.getBukti());
                    bundle.putString("desc", context.getString(R.string.field_proof));
                    Intent i = new Intent(context, ImageViewerActivity.class);
                    i.putExtras(bundle);
                    context.startActivity(i);
                }
            });
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dafulList.size();
    }
}