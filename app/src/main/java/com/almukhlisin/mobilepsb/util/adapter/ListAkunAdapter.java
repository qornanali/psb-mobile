package com.almukhlisin.mobilepsb.util.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.api.RestApi;
import com.almukhlisin.mobilepsb.model.data.Akun;
import com.almukhlisin.mobilepsb.model.data.Result;
import com.almukhlisin.mobilepsb.util.ApiCallback;
import com.almukhlisin.mobilepsb.util.RetrofitBuilder;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;

public abstract class ListAkunAdapter extends RecyclerView.Adapter<ListAkunAdapter.akunItemHolder> {

    private List<Akun> akunList;
    private Context context;
    private View itemView;


    class akunItemHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_akun_email)
        TextView tvAkunEmail;
        @Bind(R.id.tv_akun_fullname)
        TextView tvAkunFullname;
        @Bind(R.id.tv_akun_level)
        TextView tvAkunLevel;
        @Bind(R.id.tv_akun_nohp)
        TextView tvAkunNohp;
        @Bind(R.id.tv_akun_username)
        TextView tvAkunUsername;
        @Bind(R.id.btn_do_remove)
        Button btnDoRemove;

        Akun item;

        public akunItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ListAkunAdapter(Context context, List<Akun> akunList) {
        this.akunList = akunList;
        this.context = context;
    }

    @Override
    public akunItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_akun, parent, false);

        return new akunItemHolder(itemView);
    }

    public abstract void onItemClick(int position);

    @Override
    public void onBindViewHolder(final akunItemHolder holder, final int position) {
        holder.item = akunList.get(position);

        holder.tvAkunEmail.setText(
                context.getString(R.string.field_email) + " : " + holder.item.getEmail()
        );
        holder.tvAkunFullname.setText(
                holder.item.getNamaLengkap()
        );
        holder.tvAkunLevel.setText(
                context.getString(R.string.field_level) + " : " + holder.item.getLevel()
        );
        holder.tvAkunNohp.setText(
                context.getString(R.string.field_phone_number) + " : " + holder.item.getNoHp()
        );
        holder.tvAkunUsername.setText(
                context.getString(R.string.field_username) + " : " + holder.item.getUsername()
        );

                holder.btnDoRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        RestApi restApi = RetrofitBuilder.create(RestApi.class);
                        Call<Result> resultCall = restApi.postDeleteAkun(
                                holder.item.getUsername()
                        );
                        resultCall.enqueue(new ApiCallback<Result>() {
                            @Override
                            public void onSuccess(Result data) {
                                Toast.makeText(context, data.getMessage(), Toast.LENGTH_LONG).show();
                                if(data.getMessage().equals("sukses")){
                                    akunList.remove(position);
                                    notifyItemRemoved(position);
                                }
                            }

                            @Override
                            public void onFailed(Throwable t) {

                            }
                        });
                    }
                });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return akunList.size();
    }
}