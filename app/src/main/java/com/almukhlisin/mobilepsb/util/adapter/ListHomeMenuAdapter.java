package com.almukhlisin.mobilepsb.util.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.model.other.HomeMenu;

public abstract class ListHomeMenuAdapter extends RecyclerView.Adapter<ListHomeMenuAdapter.HomeMenuItemHolder> {

    private List<HomeMenu> homeMenus;
    private Context context;
    private View itemView;


    class HomeMenuItemHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.btn_dashboard_home_menu)
        Button btnHomeMenu;

        public HomeMenuItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ListHomeMenuAdapter(Context context, List<HomeMenu> homeMenus) {
        this.homeMenus = homeMenus;
        this.context = context;
    }

    @Override
    public HomeMenuItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home_menu, parent, false);

        return new HomeMenuItemHolder(itemView);
    }

    public abstract void onItemClick(int position);

    @Override
    public void onBindViewHolder(HomeMenuItemHolder holder, final int position) {
        holder.btnHomeMenu.setText(
                homeMenus.get(position).getText()
        );
        holder.btnHomeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeMenus.size();
    }
}