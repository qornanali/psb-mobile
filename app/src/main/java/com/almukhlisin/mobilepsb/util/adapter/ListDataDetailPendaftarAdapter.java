package com.almukhlisin.mobilepsb.util.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.model.other.DataDetailPendaftar;
import com.almukhlisin.mobilepsb.model.other.HomeMenu;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListDataDetailPendaftarAdapter extends RecyclerView.Adapter<ListDataDetailPendaftarAdapter.DataDetailPendaftarHolder> {

    private List<DataDetailPendaftar> dataDetailPendaftars;
    private Context context;
    private View itemView;


    class DataDetailPendaftarHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_data_detail_pendaftar_content)
        TextView tvDataDetailPendaftarContent;
        @Bind(R.id.tv_data_detail_pendaftar_field)
        TextView tvDataDetailPendaftarField;

        public DataDetailPendaftarHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ListDataDetailPendaftarAdapter(Context context, List<DataDetailPendaftar> dataDetailPendaftars) {
        this.dataDetailPendaftars = dataDetailPendaftars;
        this.context = context;
    }

    @Override
    public DataDetailPendaftarHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_data_detail_pendaftar, parent, false);

        return new DataDetailPendaftarHolder(itemView);
    }


    @Override
    public void onBindViewHolder(DataDetailPendaftarHolder holder, final int position) {
        holder.tvDataDetailPendaftarContent.setText(
                dataDetailPendaftars.get(position).getContent()
        );
        holder.tvDataDetailPendaftarField.setText(
                dataDetailPendaftars.get(position).getField()
        );
    }

    @Override
    public int getItemCount() {
        return dataDetailPendaftars.size();
    }
}