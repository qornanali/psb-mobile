package com.almukhlisin.mobilepsb.util;

import android.support.annotation.Nullable;
import android.support.compat.BuildConfig;
import android.util.Log;

import com.almukhlisin.mobilepsb.util.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public abstract class ApiCallback<T> implements Callback<T> {

    public abstract void onSuccess(T data);
    public abstract void onFailed(Throwable t);

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if(response.body() != null){
            Logger.log(Log.INFO, "data -> " + response.body().toString());
            onSuccess(response.body());
        }else{
            Logger.log(Log.ERROR, response.toString());
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if(!BuildConfig.DEBUG){
            Logger.log(t);
        }
        onFailed(t);
    }
}
