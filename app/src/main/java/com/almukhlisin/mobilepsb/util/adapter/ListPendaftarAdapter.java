package com.almukhlisin.mobilepsb.util.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.almukhlisin.mobilepsb.BuildConfig;
import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.model.data.Pendaftar;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public abstract class ListPendaftarAdapter extends RecyclerView.Adapter<ListPendaftarAdapter.PendaftarItemHolder> {

    private List<Pendaftar> pendaftarList;
    private Context context;
    private View itemView;


    class PendaftarItemHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.iv_pendaftar_pas_foto)
        ImageView ivPendaftarPasFoto;
        @Bind(R.id.tv_pendaftar_id_calon)
        TextView tvPendaftarIdCalon;
        @Bind(R.id.tv_pendaftar_nama_calon)
        TextView tvPendaftarNamaCalon;
        @Bind(R.id.tv_pendaftar_sekolah)
        TextView tvPendaftarSekolah;
        @Bind(R.id.tv_pendaftar_status_pendaftaran)
        TextView tvPendaftarStatusPendaftaran;

        Pendaftar item;

        public PendaftarItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ListPendaftarAdapter(Context context, List<Pendaftar> pendaftarList) {
        this.pendaftarList = pendaftarList;
        this.context = context;
    }

    @Override
    public PendaftarItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pendaftar, parent, false);

        return new PendaftarItemHolder(itemView);
    }

    public abstract void onItemClick(int position);

    @Override
    public void onBindViewHolder(PendaftarItemHolder holder, final int position) {
        holder.item = pendaftarList.get(position);

        Picasso.with(context).load(BuildConfig.BASE_URL+holder.item.getPasFoto())
                .resize(80, 80).centerCrop()
                .error(R.color.gray_light)
                .into(holder.ivPendaftarPasFoto);
        holder.tvPendaftarIdCalon.setText(
                context.getString(R.string.id_calon) + " : " + holder.item.getIdCalon()
        );
        holder.tvPendaftarNamaCalon.setText(
                holder.item.getNamaCalon()
        );
        holder.tvPendaftarStatusPendaftaran.setText(
                holder.item.getStatusPendaftaran()
        );
        holder.tvPendaftarSekolah.setText(
                context.getString(R.string.field_school) + " : " + holder.item.getSekolahAsal()
        );
        holder.tvPendaftarStatusPendaftaran.setTextColor(
                context.getResources().getColor(
                        holder.item.getStatusPendaftaran().equals("Sudah Dikonfirmasi")
                        ? R.color.green : R.color.red)
        );
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pendaftarList.size();
    }
}