package com.almukhlisin.mobilepsb.util.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.almukhlisin.mobilepsb.BuildConfig;
import com.almukhlisin.mobilepsb.R;
import com.almukhlisin.mobilepsb.model.other.Attachment;
import com.almukhlisin.mobilepsb.util.Logger;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public abstract class ListUploadAttachmentAdapter extends RecyclerView.Adapter<ListUploadAttachmentAdapter.AttachmentItemHolder> {

    private List<Attachment> attachments;
    private Context context;
    private View itemView;

    class AttachmentItemHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.iv_attachment)
        ImageView ivAttachment;
        @Bind(R.id.tv_attachment_desc)
        TextView tvAttachment;
        @Bind(R.id.rl_attachment)
        RelativeLayout rlAttachment;
        @Bind(R.id.btn_do_upload_attachment)
        Button btnDoUploadAttachment;

        Attachment item;

        public AttachmentItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ListUploadAttachmentAdapter(Context context, List<Attachment> attachments) {
        this.attachments = attachments;
        this.context = context;
    }

    @Override
    public AttachmentItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_upload_attachment, parent, false);

        return new AttachmentItemHolder(itemView);
    }

    public abstract void onItemClick(int position);

    @Override
    public void onBindViewHolder(final AttachmentItemHolder holder, final int position) {
        holder.item = attachments.get(position);
        holder.tvAttachment.setText(
                holder.item.getDescription()
        );
        if(!holder.item.getUrl().isEmpty()){
            Picasso.with(context).load(BuildConfig.BASE_URL+holder.item.getUrl())
                    .into(holder.ivAttachment);
        }else if(holder.item.getFile() != null){
            Picasso.with(context).load(holder.item.getFile())
                    .into(holder.ivAttachment);
        }
        holder.btnDoUploadAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick(position);
            }
        });
    }

    public void setItem(int position, Attachment attachment){
        attachments.set(position, attachment);
        notifyItemChanged(position);
    }

    public Attachment getItem(int position) {
        return attachments.get(position);
    }

    @Override
    public int getItemCount() {
        return attachments.size();
    }
}