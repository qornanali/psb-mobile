package com.almukhlisin.mobilepsb.util;


public abstract class Callback {

    public abstract void doAction();
}
