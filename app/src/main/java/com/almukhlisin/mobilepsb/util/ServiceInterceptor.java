package com.almukhlisin.mobilepsb.util;

import android.util.Log;

import java.io.IOException;

import com.almukhlisin.mobilepsb.BuildConfig;
import com.almukhlisin.mobilepsb.util.Logger;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ServiceInterceptor implements Interceptor {

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request original = chain.request();

        Request request = original;
        long t1 = System.nanoTime();
        Logger.log(Log.INFO, String.format("Sending request %s on %s%n%s",
                request.url(), chain.connection(), request.headers()));

        Response response = chain.proceed(request);
        long t2 = System.nanoTime();

        Logger.log(Log.INFO, String.format("Received response for %s in %.1fms%n%s %s",
                response.request().url(), (t2 - t1) / 1e6d, response.headers(), response.toString()));

        return response;
    }
}
